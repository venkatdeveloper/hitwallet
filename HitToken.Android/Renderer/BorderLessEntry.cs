﻿using System;
using Android.Content;
using HitToken.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(BorderLessEntry))]
namespace HitToken.Droid.Renderer
{
    public class BorderLessEntry : EntryRenderer
    {
        public BorderLessEntry(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if(Control == null)
            {
                return;
            }
            Control.SetBackground(null);
        }
    }
}
