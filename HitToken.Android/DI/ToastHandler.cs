﻿using Android.Widget;
using HitToken.Droid.DI;
using HitToken.Services.Toast;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastHandler))]
namespace HitToken.Droid.DI
{
    public class ToastHandler : IToastService
    {
        public void ShowToast(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Short).Show();
        }
    }
}