﻿using HitToken.iOS.CustomViews;
using HitToken.iOS.DI;
using HitToken.Services.Toast;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastHandler))]
namespace HitToken.iOS.DI
{
    public class ToastHandler : IToastService
    {
        public void ShowToast(string message)
        {
            Toast.Shared.Show(message, ToastDuration.Short);
        }
    }
}