﻿using System.ComponentModel;
using HitToken.iOS.Renderers;
using HitToken.Renderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomContentPage), typeof(ExtendedPageRenderer))]
namespace HitToken.iOS.Renderers
{
    public class ExtendedPageRenderer : PageRenderer
    {
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            UpdateBackroundImage();
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Element.PropertyChanged += OnPropertyChanged;
        }

        void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == VisualElement.WidthProperty.PropertyName
            || e.PropertyName == VisualElement.HeightProperty.PropertyName)
                UpdateBackroundImage();
        }

        void UpdateBackroundImage()
        {
            var page = Element as CustomContentPage;

            if (string.IsNullOrEmpty(page.BackgroundImage) || string.IsNullOrWhiteSpace(page.BackgroundImage))
                return;

            UIGraphics.BeginImageContext(View.Frame.Size);


            UIImage image = UIImage.FromBundle(page.BackgroundImage);
            if (image == null) return;

            image = image.Scale(View.Frame.Size);
            View.BackgroundColor = UIColor.FromPatternImage(image);
        }
    }
}