﻿using System;
using HitToken.iOS.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Entry), typeof(BorderLessEntry))]
namespace HitToken.iOS.Renderer
{
    public class BorderLessEntry : EntryRenderer
    {
        public BorderLessEntry()
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;
            Control.BorderStyle = UIKit.UITextBorderStyle.None;
        }
    }
}
