﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.ChangePassword.ViewModel
{
    public class ChangePasswordViewModel:ViewModelBase
    {
        bool isPassword = true;
        public bool IsPassword
        {
            get => isPassword;
            set
            {
                isPassword = value;
                OnPropertyChanged();
            }
        }

        string passwordImage = "eye";
        public string PasswordImage
        {
            get => passwordImage;
            set
            {
                passwordImage = value;
                OnPropertyChanged();
            }
        }


        string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }




        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();





        public ICommand ConfirmPasswordTextCmd => new AsyncCommand(ConfirmPasswordText);


        async Task ConfirmPasswordText()
        {
            if (IsPassword)
            {
                IsPassword = false;
                PasswordImage = "eyehide";
            }
            else
            {
                IsPassword = true;
                PasswordImage = "eye";
            }
            await Task.CompletedTask;
        }


        public ChangePasswordViewModel()
        {
        }


        public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);
        async Task InitialSetup(object data)
        {
            if (data != null)
            {
                Name = data.ToString();
            }
            await Task.CompletedTask;
        }
    }
}
