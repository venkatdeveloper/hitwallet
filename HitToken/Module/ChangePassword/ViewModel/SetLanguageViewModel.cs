﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.ChangePassword.ViewModel
{
    public class SetLanguageViewModel:ViewModelBase
    {

        bool canshowLanguage;
        public bool CanshowLanguage
        {
            get => canshowLanguage;
            set
            {
                canshowLanguage = value;
                OnPropertyChanged();
            }
        }


        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

        public ICommand ShowLanguageCmd => new AsyncCommand(ShowLanguage);


        async Task ShowLanguage()
        {
            if (CanshowLanguage)
            {
                CanshowLanguage = false;
                
            }
            else
            {
                CanshowLanguage = true;
             
            }
            await Task.CompletedTask;
        }

        public SetLanguageViewModel()
        {
        }
    }
}
