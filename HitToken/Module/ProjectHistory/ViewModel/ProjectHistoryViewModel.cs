﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.PojectHistory.ViewModel
{
    public class ProjectHistoryViewModel:ViewModelBase
    {
        public ProjectHistoryViewModel()
        {
        }

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();
    }
}
