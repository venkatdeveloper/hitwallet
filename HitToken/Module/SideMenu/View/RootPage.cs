﻿using HitToken.Module.Common;
using HitToken.Module.SideMenu.ViewModel;
using Xamarin.Forms;

namespace HitToken.Module.SideMenu.View
{
    public class RootPage : MasterDetailPage
    {
        public RootPage()
        {
            Master = new SideMenuPage();
            Detail = new Page { BackgroundColor = Color.Transparent };
        }

        protected override void OnAppearing()
        {
            MessagingCenter.Instance.Subscribe<SideMenuViewModel>(this, Constants.HIDE_MASTER_KEY, (obj) => { IsPresented = false; });
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Instance.Unsubscribe<SideMenuViewModel>(this, Constants.HIDE_MASTER_KEY);
            base.OnDisappearing();
        }
    }
}