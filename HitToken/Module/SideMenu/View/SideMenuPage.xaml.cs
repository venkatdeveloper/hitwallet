﻿using HitToken.Module.SideMenu.ViewModel;
using HitToken.Renderer;
using Xamarin.Forms;

namespace HitToken.Module.SideMenu.View
{
    public partial class SideMenuPage : ContentPage
    {
        public SideMenuPage()
        {
            InitializeComponent();
            BindingContext = new SideMenuViewModel();
        }
    }
}