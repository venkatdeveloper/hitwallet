﻿namespace HitToken.Module.SideMenu.Model
{
    public enum MenuType
    {

        Dashboard,
        Project,
        Refferal,
        Payment,
        ProjectHistory,
        MultiConvert,
        Settings,
        Faqs,
        Support,
        CheckAppUpdate,
        Logout
    }
}