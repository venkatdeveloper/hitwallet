﻿using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Settings.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.SideMenu.ViewModel
{
    public class SideMenuHeaderViewModel : ViewModelBase
    {
        public string UserName { get; set; } = AppSettings.UserName;
        public ICommand ProfileCmd => new AsyncCommand(() => NavigationService.NavigateToAsync<SettingsViewModel>());
    }
}