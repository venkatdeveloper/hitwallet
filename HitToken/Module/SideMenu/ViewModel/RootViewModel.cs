﻿using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Module.DashBoard.ViewModel;

namespace HitToken.Module.SideMenu.ViewModel
{
    public class RootViewModel : ViewModelBase
    {
        public override Task InitializeAsync(object navigationData) => NavigationService.NavigateToAsync(typeof(DashBoardViewModel), true);
    }
}