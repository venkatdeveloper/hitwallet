﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Module.SideMenu.Model;
using Xamarin.Forms;
using System.Windows.Input;
using HitToken.Module.Login.ViewModel;
using HitToken.Utils;
using HitToken.Services.Dialog;
using HitToken.Module.Support.ViewModel;
using HitToken.Module.DashBoard.ViewModel;
using HitToken.Module.ProjectWallet.ViewModel;
using HitToken.Module.Referral.ViewModel;
using HitToken.Module.Payment.ViewModel;
using HitToken.Module.PojectHistory.ViewModel;
using HitToken.Module.MultiConvert.ViewModel;
using HitToken.Module.Settings.ViewModel;

namespace HitToken.Module.SideMenu.ViewModel
{
    public class SideMenuViewModel : ViewModelBase
    {
        readonly IDialogService dialogService;

        SideMenuHeaderViewModel header;
        public SideMenuHeaderViewModel Header
        {
            get => header;
            set
            {
                header = value;
                OnPropertyChanged();
            }
        }

        List<MenuViewModel> menuList;
        public List<MenuViewModel> MenuList
        {
            get => menuList;
            set
            {
                menuList = value;
                OnPropertyChanged();
            }
        }

        MenuViewModel selectedMenuItem;
        public MenuViewModel SelectedMenuItem
        {
            get => selectedMenuItem;
            set
            {
                selectedMenuItem = value;
                OnPropertyChanged();
                if (value != null && !IsBusy)
                {
                    _ = OnMenuItemSelected();
                }
            }
        }

        public ICommand SignOutCmd => new AsyncCommand(OnSignOut);

        public SideMenuViewModel()
        {
            dialogService = Locator.Instance.Resolve<IDialogService>();
            Header = new SideMenuHeaderViewModel();
            SetupMenuData();
        }

        void SetupMenuData()
        {
            MenuList = new List<MenuViewModel> {
                    new MenuViewModel {
                        Title = "Dashboard",
                        ImageUnSelect = "dashboard",
                        ImageSelect = "dashboard",
                        IsSelected = true,
                        MenuType = MenuType.Dashboard,
                        ViewModelType = typeof(DashBoardViewModel)

                       
                    },
                        new MenuViewModel {
                        Title = "Project",
                        ImageUnSelect = "project",
                        ImageSelect = "project",
                        IsSelected = false,
                        MenuType = MenuType.Project,
                        ViewModelType = typeof(ProjectWalletViewModel)
                    },
                    new MenuViewModel {
                        Title = "Refferal",
                        ImageUnSelect = "referral",
                        ImageSelect = "referral",
                        IsSelected = false,
                        MenuType = MenuType.Refferal,
                        ViewModelType = typeof(ReferralViewModel)
                    },
                    new MenuViewModel {
                        Title = "Payment",
                        ImageUnSelect = "payment",
                        ImageSelect = "payment",
                        IsSelected = false,
                        MenuType = MenuType.Payment,
                        ViewModelType = typeof(PaymentViewModel)
                    },
                    new MenuViewModel {
                        Title = "Project History",
                        ImageUnSelect = "history",
                        ImageSelect = "history",
                        IsSelected = false,
                        MenuType = MenuType.ProjectHistory,
                        ViewModelType = typeof(ProjectHistoryViewModel)
                    },
                    new MenuViewModel {
                        Title = "Multi-Convert",
                        ImageUnSelect = "convert",
                        ImageSelect = "convert",
                        IsSelected = false,
                        MenuType = MenuType.MultiConvert,
                        ViewModelType = typeof(MultiConvertViewModel)
                    },
                    new MenuViewModel {
                        Title = "Settings",
                        ImageUnSelect = "settings",
                        ImageSelect = "settings",
                        IsSelected = false,
                        MenuType = MenuType.Settings,
                        ViewModelType = typeof(SettingsViewModel)
                    },
                    new MenuViewModel {
                        Title = "Faqs",
                        ImageUnSelect = "faqs",
                        ImageSelect = "faqs",
                        IsSelected = false,
                        MenuType = MenuType.Faqs,
                        ViewModelType = typeof(SettingsViewModel)
                    },
                    new MenuViewModel {
                        Title = "Support",
                        ImageUnSelect = "support",
                        ImageSelect = "support",
                        IsSelected = false,
                        MenuType = MenuType.Support,
                        ViewModelType = typeof(SupportViewModel)
                    },

                    new MenuViewModel {
                        Title = "Check App Update",
                        ImageUnSelect = "check_app_update",
                        ImageSelect = "check_app_update",
                        IsSelected = false,
                        MenuType = MenuType.CheckAppUpdate,
                        ViewModelType = typeof(SettingsViewModel)
                    },

                    new MenuViewModel {
                        Title = "Logout",
                        ImageUnSelect = "logout",
                        ImageSelect = "logout",
                        IsSelected = false,
                        MenuType = MenuType.Logout,
                    //ViewModelType = typeof(SupportViewModel)
                    }, };
            //MenuList = new List<MenuViewModel> {
            //    new MenuViewModel {
            //        Title = "Dashboard",
            //        ImageUnSelect = "home",
            //        ImageSelect = "home_select",
            //        IsSelected = true,
            //        MenuType = MenuType.Dashboard,
            //        ViewModelType = typeof(DashboardViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "XRP Package History",
            //        ImageUnSelect = "package_history",
            //        ImageSelect = "package_history_select",
            //        IsSelected = false,
            //        MenuType = MenuType.PackageHistory,
            //        ViewModelType = typeof(PackageHistoryViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Direct Sponsor",
            //        ImageUnSelect = "sponsor",
            //        ImageSelect = "sponsor_select",
            //        IsSelected = false,
            //        MenuType = MenuType.DirectSponsor,
            //        ViewModelType = typeof(DirectSponsorViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Team List",
            //        ImageUnSelect = "team",
            //        ImageSelect = "team_select",
            //        IsSelected = false,
            //        MenuType = MenuType.TeamList,
            //        ViewModelType = typeof(TeamListViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Sponsor Tree",
            //        ImageUnSelect = "sponsor_tree",
            //        ImageSelect = "sponsor_tree_select",
            //        IsSelected = false,
            //        MenuType = MenuType.SponsorTree,
            //        ViewModelType = typeof(SponsorTreeViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Matching Tree",
            //        ImageUnSelect = "team_matching_bonus",
            //        ImageSelect = "team_matching_bonus_select",
            //        IsSelected = false,
            //        MenuType = MenuType.MatchingTree,
            //        ViewModelType = typeof(MatchingTreeViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Bonus Summary",
            //        ImageUnSelect = "bonus_summary",
            //        ImageSelect = "bonus_summary_select",
            //        IsSelected = false,
            //        MenuType = MenuType.BonusSummary,
            //        ViewModelType = typeof(BonusSummaryViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Affiliate",
            //        ImageUnSelect = "share",
            //        ImageSelect = "share_select",
            //        IsSelected = false,
            //        MenuType = MenuType.AffiliateLink,
            //        ViewModelType = typeof(AffiliateViewModel)
            //    },
            //    new MenuViewModel {
            //        Title = "Support",
            //        ImageUnSelect = "support",
            //        ImageSelect = "support_select",
            //        IsSelected = false,
            //        MenuType = MenuType.Support,
            //        ViewModelType = typeof(SupportViewModel)
            //    }
            //};
        }

        async Task OnMenuItemSelected()
        {
            HideSideMenu();
            var previousSelect = MenuList.Find(obj => obj.IsSelected);
            if (previousSelect == SelectedMenuItem)
            {
                SelectedMenuItem = null;
                return;
            }
            previousSelect.IsSelected = false;
            SelectedMenuItem.IsSelected = true;

            if (SelectedMenuItem.MenuType == MenuType.Logout)
            {
                OnSignOut();
            }


            if (SelectedMenuItem.MenuType == MenuType.Faqs || SelectedMenuItem.MenuType == MenuType.CheckAppUpdate)
            {
                ToastService.ShowToast("Coming soon");
                SelectedMenuItem = null;
                return;
            }
            await NavigationService.NavigateToAsync(SelectedMenuItem.ViewModelType, true);
            SelectedMenuItem = null;
        }

        async Task OnSignOut()
        {
            HideSideMenu();
            var accepted = await dialogService.ShowConfimationAsync(Constants.CONFIRMATION, Constants.SIGN_OUT_CONFIRMATION_MESSAGE, Constants.OK, Constants.CANCEL);
            if (accepted)
            {
                AppSettings.Clear();
                await NavigationService.NavigateToAsync<LoginViewModel>();
            }
        }

        void HideSideMenu() => MessagingCenter.Instance.Send(this, Constants.HIDE_MASTER_KEY);
    }
}