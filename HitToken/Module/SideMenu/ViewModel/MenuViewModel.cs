﻿using System;
using HitToken.Module.Common;
using HitToken.Module.SideMenu.Model;
using Xamarin.Forms;

namespace HitToken.Module.SideMenu.ViewModel
{
    public class MenuViewModel : ViewModelBase
    {
        public string Title { get; set; }
        public string ImageSelect { get; set; }
        public string ImageUnSelect { get; set; }
        public MenuType MenuType { get; set; }
        public Type ViewModelType { get; set; }
        bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set
            {
                isSelected = value;
                OnPropertyChanged(nameof(Image));
                OnPropertyChanged(nameof(TextColor));
                OnPropertyChanged(nameof(BackgroundColor));
            }
        }
        public string Image => IsSelected ? ImageSelect : ImageUnSelect;
        public Color TextColor => Color.White;
       // public Color TextColor => IsSelected ? Color.White : (Color)Application.Current.Resources["PrimaryColor"];
        public Color BackgroundColor => Color.Transparent;
    }
}


