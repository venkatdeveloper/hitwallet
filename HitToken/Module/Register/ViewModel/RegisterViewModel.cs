﻿using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Extension;
using HitToken.Module.Common;
using HitToken.Module.Login.ViewModel;
using HitToken.Module.Phrase.ViewModel;
using HitToken.Module.Register.Model;
using HitToken.Services.Authendication;
using HitToken.Utils;

namespace HitToken.Module.Register.ViewModel
{
    public class RegisterViewModel : ViewModelBase
    {
        readonly IAuthendicationServices authendicationServices;
        string referralCode = string.Empty;
        public string ReferralCode
        {
            get => referralCode;
            set
            { 
                    referralCode = value;
                    OnPropertyChanged();
                if (ReferralCode.IsValid())
                    {
                    ImageChange = "right_round";

                }
                else
                {
                    ImageChange = "clear";
                }
            }
        }

        string imageChange = string.Empty;
        public string ImageChange
        {
            get => imageChange;
            set
            {

                imageChange= value;
                OnPropertyChanged();
            }
        }


        string fullName;
        public string FullName
        {
            get => fullName;
            set
            {
               
                fullName = value;
                OnPropertyChanged();
            }
        }

        string userName;
        public string UserName
        {
            get => userName;
            set
            {
                if (userName != value)
                {
                    userName = value;
                    OnPropertyChanged();
                }
            }
        }

        string password;
        public string Password
        {
            get => password;
            set
            {
                if (password != value)
                {
                    password = value;
                    OnPropertyChanged();
                }
            }
        }
        string confirmPassword;
        public string ConfirmPassword
        {
            get => confirmPassword;
            set
            {
                if (confirmPassword != value)
                {
                    confirmPassword = value;
                    OnPropertyChanged();
                }
            }
        }
        string paymentPassword;
        public string PaymentPassword
        {
            get => paymentPassword;
            set
            {
                if (paymentPassword != value)
                {
                    paymentPassword = value;
                    OnPropertyChanged();
                }
            }
        }
        string confirmPaymentPassword;
        public string ConfirmPaymentPassword
        {
            get => confirmPaymentPassword;
            set
            {
                if (confirmPaymentPassword != value)
                {
                    confirmPaymentPassword = value;
                    OnPropertyChanged();
                }
            }
        }
        bool referralEntryStack = true;
        public bool ReferralEntryStack
        {
            get => referralEntryStack;
            set
            {
                referralEntryStack = value;
                OnPropertyChanged();
            }
        }
        bool emailStack;
        public bool EmailStack
        {
            get => emailStack;
            set
            {
                emailStack = value;
                OnPropertyChanged();
            }
        }
        bool verifyEmailStack;
        public bool VerifyEmailStack
        {
            get => verifyEmailStack;
            set
            {
                verifyEmailStack = value;
                OnPropertyChanged();
            }
        }
        bool loginPasswordStack;
        public bool LoginPasswordStack
        {
            get => loginPasswordStack;
            set
            {
                loginPasswordStack = value;
                OnPropertyChanged();
            }
        }
        bool paymentPasswordStack;
        public bool PaymentPasswordStack
        {
            get => paymentPasswordStack;
            set
            {
                paymentPasswordStack = value;
                OnPropertyChanged();
            }
        }


     
        //bool phraseStack;
        //public bool PhraseStack
        //{
        //    get => phraseStack;
        //    set
        //    {
        //        phraseStack = value;
        //        OnPropertyChanged();
        //    }
        //}
        //bool greetingsStack;
        //public bool GreetingsStack
        //{
        //    get => greetingsStack;
        //    set
        //    {
        //        greetingsStack = value;
        //        OnPropertyChanged();
        //    }
        //}
        
        bool errorMessage;
        public bool ErrorMessage
        {
            get => errorMessage;
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }
        string confirmEmail;
        public string ConfirmEmail
        {
            get => confirmEmail;
            set
            {
                confirmEmail = value;
                OnPropertyChanged();
            }
        }
        string email;
        public string Email
        {
            get => email;
            set
            {
                email = value;
                OnPropertyChanged();
            }
        }
        string verificationCode;
        public string VerificationCode
        {
            get => verificationCode;
            set
            {
                verificationCode = value;
                OnPropertyChanged();
            }
        }


        string referid;
        public string Referid
        {
            get => referid;
            set
            {
                referid = value;
                OnPropertyChanged();
                ErrorMessage = false;
            }
        }


        public ICommand SignInCmd => new AsyncCommand(NavigateToLogin);
        public ICommand SignUpCmd => new AsyncCommand(OnRegister);
        public ICommand ClearCmd => new AsyncCommand(OnClear);
        public ICommand ShowNextCmd => new AsyncCommand(ShowNextStack);
        public ICommand ResendEmailCmd => new AsyncCommand(ResendEmail);
        
        public RegisterViewModel(IAuthendicationServices authendicationServices)
        {
            this.authendicationServices = authendicationServices;
        }
        async Task NavigateToLogin()
        {
            await NavigationService.NavigateBackAsync();
        }
        async Task OnRegister()
        {
            if (!NetworkUtils.IsConnected)
                return;
            if (!IsValidate()) return;
            //IsBusy = true;
            //var token = Xamarin.Forms.DependencyService.Get<IFirebaseTokenService>().Token;
            //var response = await authendicationServices.RegisterAsync(UserName, Password, FullName, "", "");
            //IsBusy = false;
            //if (!response.IsSuccess())
            //{
            //    ToastService.ShowToast(response.Message);
            //    return;
            //}
            //AppSettings.UserName = response.Name;
            //AppSettings.IsLogin = true;
            //ToastService.ShowToast(Constants.SUCCESS);

          
            await NavigationService.NavigateToAsync<LoginViewModel>();
        }
        async Task OnClear(object data)
        {
            if (data.ToString() == "Referral")
            {
                ReferralCode = string.Empty;
            }
            //else
            //{
            //    EmailID = string.Empty;
            //}
            await Task.CompletedTask;
        }

        async Task ShowNextStack(object data)
        {
            switch (data.ToString())
            {
                case "Email":
                    {
                        //if (!IsValidReferidData()) return;
                        if (!NetworkUtils.IsConnected)
                        {
                            ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                            return;
                        }


                        //IsBusy = true;


                        //var requestModel = new RegisterRequestModel
                        //{
                        //    Referral = ReferralCode
                        //};
                        //var response = await authendicationServices.RegisterAsync(requestModel, 1);

                        //IsBusy = false;
                        //if (!response.IsSuccess())
                        //{
                        //    ToastService.ShowToast(response.Message);
                        //    return;
                        //}
                        ErrorMessage = true;
                        ReferralEntryStack = false;
                        EmailStack = true;

                        break;
                    }
                case "VerifyEmail":
                    {
                        //if (!IsValidData()) return;
                        if (!NetworkUtils.IsConnected)
                        {
                            ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                            return;
                        }

                        //var requestModel = new RegisterRequestModel
                        //{
                        //    Email = Email
                        //};
                        //var response = await authendicationServices.RegisterAsync(requestModel, 2);
                        //IsBusy = false;
                        //if (!response.IsSuccess())
                        //{
                        //    ToastService.ShowToast(response.Message);
                        //    return;
                        //}


                        EmailStack = false;
                        VerifyEmailStack = true;
                        break;
                    }
                case "LoginPassword":
                    {

                        //if (!IsValidVerifyCode()) return;
                        if (!NetworkUtils.IsConnected)
                        {
                            ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                            return;
                        }
                        //var requestModel = new RegisterRequestModel
                        //{
                        //    Smscode = VerificationCode
                        //};
                        //var response = await authendicationServices.RegisterAsync(requestModel, 3);
                        //IsBusy = false;
                        //if (!response.IsSuccess())
                        //{
                        //    ToastService.ShowToast(response.Message);
                        //    return;
                        //}
                        VerifyEmailStack = false;
                        LoginPasswordStack = true;
                        break;
                    }
                case "PaymentPassword":
                    {

                        //if (!IsValidPasswordData()) return;
                        if (!NetworkUtils.IsConnected)
                        {
                            ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                            return;
                        }
                        //var requestModel = new RegisterRequestModel
                        //{
                        //    Loginpassword = Password
                        //};
                        //var response = await authendicationServices.RegisterAsync(requestModel, 4);
                        //IsBusy = false;
                        //if (!response.IsSuccess())
                        //{
                        //    ToastService.ShowToast(response.Message);
                        //    return;
                        //}

                        LoginPasswordStack = false;
                        PaymentPasswordStack = true;
                        break;
                    }
                case "PhraseStart":
                    {
                        //if (!IsValidPasswordData()) return;
                        if (!NetworkUtils.IsConnected)
                        {
                            ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                            return;
                        }

                        //var requestModel = new RegisterRequestModel
                        //{
                        //    Paymentpassword = PaymentPassword
                        //};
                        //var response = await authendicationServices.RegisterAsync(requestModel, 5);
                        //IsBusy = false;
                        //if (!response.IsSuccess())
                        //{
                        //    ToastService.ShowToast(response.Message);
                        //    return;
                        //}


                        await NavigationService.NavigateToAsync<PhraseViewModel>();
                        break;
                    }
                
            }
            await Task.CompletedTask;
        }






        //Referid stack
        bool IsValidReferidData()
        {
            var isValid = false;
            if (!Referid.IsValid())
                ToastService.ShowToast(Constants.EMPTY_REFERID);
           
         
            else isValid = true;
            return isValid;
        }

        //email Stack
        bool IsValidData()
        {
            var isValid = false;
            if (!Email.IsValid())
                ToastService.ShowToast(Constants.EMPTY_EMAIL);
            else if (!ConfirmEmail.IsValid())
                ToastService.ShowToast(Constants.EMPTY_EMAIL);
            else if (Email != ConfirmEmail)
                ToastService.ShowToast(Constants.INVALID_EMAIL);
            else isValid = true;
            return isValid;
        }

        //verifycode
        bool IsValidVerifyCode()
        {
            var isValid = false;
            if (!VerificationCode.IsValid())
                ToastService.ShowToast(Constants.EMPTY_VERIFICATION_CODE);


            else isValid = true;
            return isValid;
        }

        bool IsValidPasswordData()
        {
            var isValid = false;
            if (!Password.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else if (!ConfirmPassword.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else if (Password != ConfirmPassword)
                ToastService.ShowToast(Constants.PASSWORD_MISMATCH);
            else isValid = true;
            return isValid;
        }



        async Task ResendEmail()
        {
            ToastService.ShowToast("Done");
            await Task.CompletedTask;
        }
        bool IsValidate()
        {
            var isValid = false;
            if (!UserName.IsValidMail())
                ToastService.ShowToast(Constants.INVALID_EMAIL);            
            else if (!FullName.IsValid())
                ToastService.ShowToast(Constants.EMPTY_FULLNAME);
            else if (!Password.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else if (!ConfirmPassword.IsValid())
                ToastService.ShowToast(Constants.CONFIRM_PIN);
            else
                isValid = true;
            return isValid;
        }
    }
}
