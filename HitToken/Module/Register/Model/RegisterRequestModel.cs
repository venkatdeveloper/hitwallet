﻿using System;
using Newtonsoft.Json;

namespace HitToken.Module.Register.Model
{
    public class RegisterRequestModel
    {

       

        [JsonProperty("smscode")]
        public string Smscode { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("referral")]
        public string Referral { get; set; }

        [JsonProperty("loginpassword")]
        public string Loginpassword { get; set; }

        [JsonProperty("paymentpassword")]
        public string Paymentpassword { get; set; }







        public RegisterRequestModel()
        {
        }
    }
}
