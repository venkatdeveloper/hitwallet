﻿using System;

using Xamarin.Forms;

namespace HitToken.Module.Login.View
{
    public class LoginNavigationPage : NavigationPage
    {
        public LoginNavigationPage() {  }
        public LoginNavigationPage(Page page) : base(page) { }
    }
}

