﻿using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Extension;
using HitToken.Module.Common;
using HitToken.Services.Authendication;
using HitToken.Utils;
using ZXing.OneD.RSS;

namespace HitToken.Module.Login.ViewModel
{
    public class ForgotPasswordViewModel : ViewModelBase
    {
        readonly IAuthendicationServices authendicationServices;
        string userName;
        public string UserName
        {
            get => userName;
            set
            {
                if (userName != value)
                {
                    userName = value;
                    OnPropertyChanged();
                }
            }
        }
        bool optionsStack = true;
        public bool OptionsStack
        {
            get => optionsStack;
            set
            {
                optionsStack = value;
                OnPropertyChanged();
            }
        }
        bool cofirmationMessageStack;
        public bool CofirmationMessageStack
        {
            get => cofirmationMessageStack;
            set
            {
                cofirmationMessageStack = value;
                OnPropertyChanged();
            }
        }
        bool userNameEntryStack;
        public bool UserNameEntryStack
        {
            get => userNameEntryStack;
            set
            {
                userNameEntryStack = value;
                OnPropertyChanged();
            }
        }
        bool phraseStack;
        public bool PhraseStack
        {
            get => phraseStack;
            set
            {
                phraseStack = value;
                OnPropertyChanged();
            }
        }
        bool showOptions;
        public bool ShowOptions
        {
            get => showOptions;
            set
            {
                showOptions = value;
                OnPropertyChanged();
            }
        }
        string userID = string.Empty;
        public string UserID
        {
            get => userID;
            set
            {
                userID = value;
                OnPropertyChanged();
                if (UserID.IsValid())
                {
                    UserImage = "right_round";
                    RotationValue = "0";
                }
                else
                {
                    UserImage = "clear";
                    RotationValue = "45";
                }
            }
        }
        string userImage;
        public string UserImage
        {
            get => userImage;
            set
            {
                userImage = value;
                OnPropertyChanged();
            }
        }
        string emailID = string.Empty;
        public string EmailID
        {
            get => emailID;
            set
            {
                emailID = value;
                OnPropertyChanged();
                if (EmailID.IsValidMail())
                {
                    EmailImage = "right_round";
                    RotationValue = "0";
                }
                else
                {
                    EmailImage = "clear";
                    RotationValue = "45";
                }
            }
        }


        string emailImage;
        public string EmailImage
        {
            get => emailImage;
            set
            {
                emailImage = value;
                OnPropertyChanged();
            }
        }
        string rotationValue;
        public string RotationValue
        {
            get => rotationValue;
            set
            {
                rotationValue = value;
                OnPropertyChanged();
            }

        }




        string mnemonic;
        public string Mnemonic
        {
            get => mnemonic;
            set
            {
                mnemonic = value;
                OnPropertyChanged();

            }
        }





        //public ICommand ForgotPasswordCmd => new AsyncCommand(OnForGotPassword);
        public ICommand SubmitCmd => new AsyncCommand(OnSubmitPhrase);
        public ICommand EmailValidationCmd => new AsyncCommand(OnEmailValidation);
        public ICommand ClearCmd => new AsyncCommand(OnClear);
        public ICommand ShowOptionsCmd => new AsyncCommand(OnShowOptions);
        public ICommand ShowUserStackCmd => new AsyncCommand(OnShowUserStack);
        public ICommand GotoLoginCmd => new AsyncCommand(GotoLogin);
        public ICommand ResendEmailCmd => new AsyncCommand(ResendEmail);


        public ForgotPasswordViewModel(IAuthendicationServices authendicationServices)
        {
            this.authendicationServices = authendicationServices;
        }
        //public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);

        //async Task InitialSetup(object data)
        //{

        //}
        async Task OnShowOptions()
        {
            if (ShowOptions)
                ShowOptions = false;
            else
                ShowOptions = true;
            await Task.CompletedTask;
        }

        //async Task OnForGotPassword()
        //{
        //    if (!NetworkUtils.IsConnected)
        //    {
        //        ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
        //        return;
        //    }
        //    if (!IsValidate()) return;

        //    //IsBusy = true;

        //    //var response = await authendicationServices.LoginRequestAsync(UserName,Password,token);
        //    //IsBusy = false;
        //    //if (!response.IsSuccess())
        //    //{
        //    //    ToastService.ShowToast(response.Message);
        //    //    return;
        //    //}
        //    //ToastService.ShowToast(response.Message);

        //    await NavigationService.NavigateToAsync<LoginViewModel>();
        //}


        async Task GotoLogin()
        {


            await NavigationService.NavigateBackAsync();
        }

        async Task ResendEmail()
        {
            await OnEmailValidation();

        }


        async Task OnClear(object data)
        {
            if (data.ToString() == "User")
            {
                UserID = string.Empty;
            }
            else
            {
                EmailID = string.Empty;
            }
            await Task.CompletedTask;
        }


        async Task OnShowUserStack(object data)
        {
            if (data.ToString() == "User")
            {
                await OnEmailValidation();
                OptionsStack = false;
                CofirmationMessageStack = true;

            }
            else
            {
                //if (!IsValidateEmail()) return;
                if (!NetworkUtils.IsConnected)
                {
                    ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                    return;
                }


                //IsBusy = true;


                //var response = await authendicationServices.ForgotPasswordPhraseAsync(Mnemonic);
                //IsBusy = false;
                //if (!response.IsSuccess())
                //{
                //    ToastService.ShowToast(response.Message);
                //    return;
                //}

                OptionsStack = false;
                PhraseStack = true;
            }
            await Task.CompletedTask;
        }


        bool IsValidateEmail()
        {
            var isValid = false;
            if (!UserName.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            else if (!EmailID.IsValidMail())
                ToastService.ShowToast(Constants.INVALID_EMAIL);
            else
                isValid = true;
            return isValid;

        }



        async Task OnEmailValidation()
        {
            //if (!IsValidateEmail()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            //IsBusy = true;
            //var response = await authendicationServices.RecoverPasswordAsync(EmailID);
            //IsBusy = false;
            //if (!response.IsSuccess())
            //{
            //    ToastService.ShowToast(response.Message);
            //    return;
            //}
            await Task.CompletedTask;
        }


        async Task OnSubmitPhrase()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            //if (!IsValidatePhrase()) return;

            //IsBusy = true;

            //var response = await authendicationServices.ForgotPasswordPhraseAsync(Mnemonic);
            //IsBusy = false;
            //if (!response.IsSuccess())
            //{
            //    ToastService.ShowToast(response.Message);
            //    return;
            //}
            //ToastService.ShowToast(response.Message);
            PhraseStack = false;
            CofirmationMessageStack = true;
            await Task.CompletedTask;


        }


        bool IsValidatePhrase()
        {
            var isValid = false;
            if (!Mnemonic.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PHRASE);
            else
                isValid = true;
            return isValid;

        }





    }
}
