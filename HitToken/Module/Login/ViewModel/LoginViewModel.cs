﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Extension;
using HitToken.Module.Common;
using HitToken.Module.DashBoard.ViewModel;
using HitToken.Module.Register.ViewModel;
using HitToken.Module.SideMenu.ViewModel;
using HitToken.Services.Authendication;
using HitToken.Services.Dialog;
using HitToken.Utils;
using Plugin.Fingerprint.Abstractions;

namespace HitToken.Module.Login.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
        readonly IAuthendicationServices authendicationServices;
        readonly IDialogService dialogService;

        string userName=AppSettings.UserName;
        public string UserName
        {
            get => userName;
            set
            {
                if (userName != value)
                {
                    userName = value;
                    OnPropertyChanged();
                }
            }
        }

        string password;
        public string Password
        {
            get => password;
            set
            {
                if (password != value)
                {
                    password = value;
                    OnPropertyChanged();
                }
            }
        }

        string selectedLanguage = "ENGLISH";
        public string SelectedLanguage
        {
            get => selectedLanguage;
            set
            {
                if (selectedLanguage != value)
                {
                    selectedLanguage = value;
                    OnPropertyChanged();
                }
            }
        }

        List<string> languages;
        public List<string> Languages
        {
            get => languages;
            set
            {
                if (languages != value)
                {
                    languages = value;
                    OnPropertyChanged();
                }
            }
        }

        bool userStack = true;
        public bool UserStack
        {
            get => userStack;
            set
            {
                userStack = value;
                OnPropertyChanged();
            }
        }
        bool passwordStack;
        public bool PasswordStack
        {
            get => passwordStack;
            set
            {
                passwordStack = value;
                OnPropertyChanged();
            }
        }


        public ICommand SignInCmd => new AsyncCommand(OnLogin);
        public ICommand BackToLoginCmd => new AsyncCommand(BackToLoginScreen);
        public ICommand SignUpCmd => new AsyncCommand(NavigateToRegister);
        public ICommand ForgotPasswordCmd => new AsyncCommand(()=> NavigationService.NavigateToAsync<ForgotPasswordViewModel>());
        public ICommand ForgotUseridCmd => new AsyncCommand(() => NavigationService.NavigateToAsync<ForgotUserIdViewModel>());
        //public ICommand FingerPrintCmd => new AsyncCommand(AuthenticationAsync);
        CancellationTokenSource _cancel;



        public LoginViewModel(IAuthendicationServices authendicationServices,IDialogService dialogService)
        {
            this.authendicationServices = authendicationServices;
            this.dialogService = dialogService;

            //Languages = new List<string> { "ENGLISH", "TAMIL", "MALAYALAM", "TELUGU" };

        }
        async Task BackToLoginScreen()
        {
            UserStack = true;
            PasswordStack = false;
            await Task.CompletedTask;
        }

        async Task OnLogin()
        {

            if (!PasswordStack)
            {
                //if (!IsValidate()) return;
                if (!NetworkUtils.IsConnected)
                {
                    ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                    return;
                }


                //IsBusy = true;
                //var token = Xamarin.Forms.DependencyService.Get<IFirebaseTokenService>().Token;

                //var response = await authendicationServices.VerifyUseridAsync(UserName);
                //IsBusy = false;
                //if (!response.IsSuccess())
                //{
                //    ToastService.ShowToast(response.Message);
                //    return;
                //}
                //AppSettings.UserName = UserName;
                PasswordStack = true;
                UserStack = false;
                return;
            }
            
            //if (!IsValidatePassword()) return;
            //var response1 = await authendicationServices.MemberLoginAsync(UserName, Password);
            //if (!response1.IsSuccess())
            //{
            //    ToastService.ShowToast(response1.Message);
            //    return;
            //}
            //AppSettings.UserName = UserName;
            //AppSettings.IsLogin = true;
            await NavigationService.NavigateToAsync<RootViewModel>();
           //NavigationService.NavigateToAsync<RootViewModel>();
        }

      


        async Task NavigateToRegister()
        {
            await NavigationService.NavigateToAsync<RegisterViewModel>();
        }



        bool IsValidate()
        {
            var isValid = false;
            if (!UserName.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            else
                isValid = true;
            return isValid;
        }



        bool IsValidatePassword()
        {
            var isValid = false;

            if (!Password.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else
                isValid = true;
            return isValid;
        }


        async Task AuthenticationAsync()
        {
            string cancel = null, fallback = null, reason = "Place your finger on the finger panel";
            _cancel = new CancellationTokenSource();

            var dialogConfig = new AuthenticationRequestConfiguration(reason)
            {
                CancelTitle = cancel,
                FallbackTitle = fallback,
                UseDialog = true
            };
            var fingerAvailable = await Plugin.Fingerprint.CrossFingerprint.Current.IsAvailableAsync();
            if (fingerAvailable)
            {
                var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync
                             (dialogConfig, _cancel.Token);

                if (result.Authenticated)
                {
                    await NavigationService.NavigateToAsync<RootViewModel>();
                }
                else
                {
                    await dialogService.ShowAlertAsync("FingerPrintError", result.ErrorMessage, Constants.OK);
                }
            }
            else
            {
                await dialogService.ShowAlertAsync("FingerPrintError", "No Registered fingers in your phone.", Constants.OK);
            }
        }

    }
}
