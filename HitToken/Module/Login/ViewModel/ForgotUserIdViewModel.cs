﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Extension;
using HitToken.Module.Common;
using HitToken.Services.Authendication;
using HitToken.Utils;

namespace HitToken.Module.Login.ViewModel
{
    public class ForgotUserIdViewModel : ViewModelBase
    {
        readonly IAuthendicationServices authendicationServices;

        bool optionsStack = true;
        public bool OptionsStack
        {
            get => optionsStack;
            set
            {
                optionsStack = value;
                OnPropertyChanged();
            }
        }
        bool userNameEntryStack;
        public bool UserNameEntryStack
        {
            get => userNameEntryStack;
            set
            {
                userNameEntryStack = value;
                OnPropertyChanged();
            }
        }
        bool userNameStack;
        public bool UserNameStack
        {
            get => userNameStack;
            set
            {
                userNameStack = value;
                OnPropertyChanged();
            }
        }
        bool phraseStack;
        public bool PhraseStack
        {
            get => phraseStack;
            set
            {
                phraseStack = value;
                OnPropertyChanged();
            }
        }
        bool showOptions;
        public bool ShowOptions
        {
            get => showOptions;
            set
            {
                showOptions = value;
                OnPropertyChanged();
            }
        }
        bool cofirmationMessageStack;
        public bool CofirmationMessageStack
        {
            get => cofirmationMessageStack;
            set
            {
                cofirmationMessageStack = value;
                OnPropertyChanged();
            }
        }
        bool errorMessage;
        public bool ErrorMessage
        {
            get => errorMessage;
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }
        string userName;
        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                OnPropertyChanged();
                ErrorMessage = false;
            }
        }

        string email=string.Empty;
        public string Email
        {
            get => email;
            set
            {
                email = value;
                OnPropertyChanged();
                if (Email.IsValidMail())
                {
                    EmailImage = "right_round";
                  
                }
                else
                {
                    EmailImage = "clear";
                  
                }

            }
        }


        string emailImage;
        public string EmailImage
        {
            get => emailImage;
            set
            {
                emailImage = value;
                OnPropertyChanged();
            }
        }


        string mnemonic;
        public string Mnemonic
        {
            get => mnemonic;
            set
            {
                mnemonic = value;
                OnPropertyChanged();

            }
        }

        public ICommand ShowOptionsCmd => new AsyncCommand(OnShowOptions);
        public ICommand ShowConfirmationCmd => new AsyncCommand(OnShowConfirmation);
        public ICommand ShowMnemonicPhraseCmd => new AsyncCommand(OnShowPhrase); 
        public ICommand ShowUserStackCmd => new AsyncCommand(OnShowUserStack);
        public ICommand GotoLoginCmd => new AsyncCommand(GotoLogin);
        public ICommand ResendEmailCmd => new AsyncCommand(ResendEmail);
        public ICommand ClearCmd => new AsyncCommand(OnEmpty);
        

        public ForgotUserIdViewModel(IAuthendicationServices authendicationServices)

        {
            this.authendicationServices = authendicationServices;

        }

        async Task OnShowOptions()
        {
            if (ShowOptions)
                ShowOptions = false;
            else
                ShowOptions = true;
            await Task.CompletedTask;
        }

        async Task GotoLogin()
        {
            await NavigationService.NavigateBackAsync();
        }

        async Task OnShowUserStack(object data)
        {
            if (data.ToString() == "User")
            {
                OptionsStack = false;

              

                UserNameEntryStack = true;
            }

            else
            {
                OptionsStack = false;

                PhraseStack = true;
            }
            await Task.CompletedTask;
        }

        async Task OnShowConfirmation()
        {

            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }


            //IsBusy = true;


            //var response = await authendicationServices.RecoverUserIdAsync(Email);
            //IsBusy = false;
            //if (!response.IsSuccess())
            //{
            //    ToastService.ShowToast(response.Message);
            //    ErrorMessage = true;
            //    return;
            //}
            UserNameEntryStack = false;
            CofirmationMessageStack = true;
            await Task.CompletedTask;

        }



        async Task ResendEmail()
        {
            if (!PhraseStack)
            {
               await OnShowConfirmation();

            }

            else
            {
                await OnShowPhrase();
            }
        }

           

        
        async Task OnEmpty()
        {
            UserName = string.Empty;
            await Task.CompletedTask;
        }



        async Task OnShowPhrase()
        {

            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }


            //IsBusy = true;


            //var response = await authendicationServices.ForgotUserIdPhraseAsync(Mnemonic);
            //IsBusy = false;
            //if (!response.IsSuccess())
            //{
            //    ToastService.ShowToast(response.Message);
              
            //    return;
            //}
            PhraseStack = false;
            CofirmationMessageStack = true;
            await Task.CompletedTask;

        }

    }
}
