﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.MultiConvert.ViewModel
{
    public class MultiConvertViewModel:ViewModelBase
    {

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();
        public MultiConvertViewModel()
        {
        }
    }
}
