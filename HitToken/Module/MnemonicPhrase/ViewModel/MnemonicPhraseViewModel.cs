﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Phrase.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.MnemonicPhrase.ViewModel
{
    public class MnemonicPhraseViewModel:ViewModelBase
    {

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

        public ICommand GotoPhraseCmd => new AsyncCommand(() => NavigationService.NavigateToAsync<PhraseViewModel>());
        public MnemonicPhraseViewModel()
        {
        }
    }
}
