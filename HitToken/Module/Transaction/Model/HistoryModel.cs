﻿using System;
namespace HitToken.Module.Transaction.Model
{
    public class HistoryModel
    {
        


        public string Date { get; set; }

        public string Month { get; set; }

        public string Year { get; set; }

        public string Time { get; set; }


        public string CoinName { get; set; }


        public string CoinValue { get; set; }


        public string ConvertValue { get; set; }

        public string ConvertCoin { get; set; }



        public string Type { get; set; }

        public string TypeImage { get; set; }

        public string Address { get; set; }


    }
}