﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Transaction.Model;
using HitToken.Utils;

namespace HitToken.Module.Root.ViewModel
{
    public class ConvertHistoryViewModel:ViewModelBase
    {
        List<HistoryModel> historyList;
        public List<HistoryModel> HistoryList
        {
            get => historyList;
            set
            {
                historyList = value;
                OnPropertyChanged();
            }
        }



        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

        public ConvertHistoryViewModel()
        {
            HistoryList = new List<HistoryModel>
            {
                new HistoryModel
                {
                   Date="08",
                   Month="August",
                   Year="19",
                   Time="20:02",
                   CoinName="BTC",
                   CoinValue="20.202",
                   ConvertCoin="ETH",
                   ConvertValue="484.40",
                   
                   Type="Convert",
                    TypeImage="convertblack",
                   Address="fg8f8fgfg8e"


                },
                new HistoryModel
                {
                    Date="08",
                   Month="August",
                   Year="19",
                   Time="20:02",
                   CoinName="BTC",
                   CoinValue="20.202",
                   ConvertCoin="ETH",
                   ConvertValue="484.40",

                   Type="Convert",
                    TypeImage="convertblack",
                   Address="fg8f8fgfg8e"



                },
                new HistoryModel
                {
                    Date="08",
                   Month="August",
                   Year="19",
                   Time="20:02",
                   CoinName="BTC",
                   CoinValue="20.202",
                   ConvertCoin="ETH",
                   ConvertValue="484.40",

                   Type="Convert",
                    TypeImage="convertblack",
                   Address="fg8f8fgfg8e"



                },

                new HistoryModel
                {
                  Date="08",
                   Month="August",
                   Year="19",
                   Time="20:02",
                   CoinName="BTC",
                   CoinValue="20.202",
                   ConvertCoin="ETH",
                   ConvertValue="484.40",

                   Type="Convert",
                    TypeImage="convertblack",
                   Address="fg8f8fgfg8e"


                },

                new HistoryModel
                {
          Date="08",
                   Month="August",
                   Year="19",
                   Time="20:02",
                   CoinName="BTC",
                   CoinValue="20.202",
                   ConvertCoin="ETH",
                   ConvertValue="484.40",

                   Type="Convert",
                    TypeImage="convertblack",
                   Address="fg8f8fgfg8e"

                },




           };

        }
    }
}
