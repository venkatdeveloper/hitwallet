﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.DashBoard.Model;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Transaction.Model;
using HitToken.Utils;
using Xamarin.Essentials;

namespace HitToken.Module.Root.ViewModel
{
    public class TransactionViewModel : ViewModelBase
    {
        string qrCode = "1233";
        public string QrCode
        {
            get => qrCode;
            set
            {
                qrCode = value;
                OnPropertyChanged();
            }
        }

        bool sendView;
        public bool SendView
        {
            get => sendView;
            set
            {
                sendView = value;
                OnPropertyChanged();
            }
        }

        bool receiveView;
        public bool ReceiveView
        {
            get => receiveView;
            set
            {
                receiveView = value;
                OnPropertyChanged();
            }
        }

        bool convertView;
        public bool ConvertView
        {
            get => convertView;
            set
            {
                convertView = value;
                OnPropertyChanged();
            }
        }

        bool transactionHistoryView = true;
        public bool TransactionHistoryView
        {
            get => transactionHistoryView;
            set
            {
                transactionHistoryView = value;
                OnPropertyChanged();
            }
        }

        string estimatedFee = "0.5454";
        public string EstimatedFee
        {
            get => estimatedFee;
            set
            {
                estimatedFee = value;
                OnPropertyChanged();
            }
        }


        string totalSend = "0.4054";
        public string TotalSend
        {
            get => totalSend;
            set
            {
                totalSend = value;
                OnPropertyChanged();
            }
        }


        string hitpayFee = "0.448";
        public string HitpayFee
        {
            get => hitpayFee;
            set
            {
                hitpayFee = value;
                OnPropertyChanged();
            }
        }

        List<HistoryModel> historyList;
        public List<HistoryModel> HistoryList
        {
            get => historyList;
            set
            {
                historyList = value;
                OnPropertyChanged();
            }
        }

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

        public ICommand GotoConvertHistoryCmd => new AsyncCommand(() => NavigationService.NavigateToAsync<ConvertHistoryViewModel>());
        public ICommand ShowModulesCmd => new AsyncCommand(ShowModules);
        public ICommand CopyCmd => new AsyncCommand(OnCopy);
        public ICommand ShareCmd => new AsyncCommand(OnShare);
        public TransactionViewModel()
        {
            HistoryList = new List<HistoryModel>
            {
                new HistoryModel
                {
                   Date="08",
                   Month="August",
                   Year="19",
                   Time="20:08",
                   CoinName="BTC",
                   CoinValue="20.202",
                   Type="send",
                    TypeImage="send",
                   Address="fg8f8fgfg8e"

                },
                new HistoryModel
                {
                   Date="08",
                    Time="20:08",
                   Month="August",
                   Year="19",
                   CoinName="BTC",
                   CoinValue="20.202",
                   TypeImage="recieve",
                   Type="send",
                   Address="fg8f8fgfg8e"
                },
                new HistoryModel
                {
                   Date="08",
                    Time="20:08",
                   Month="August",
                   Year="19",
                   CoinName="BTC",
                   CoinValue="20.202",
                    TypeImage="send",
                   Type="send",
                   Address="fg8f8fgfg8e"
                },

                new HistoryModel
                {
                  Date="08",
                   Month="August",
                   Year="19",
                    Time="20:08",
                   CoinName="BTC",
                   CoinValue="20.202",
                  TypeImage="recieve",
                   Type="send",
                   Address="fg8f8fgfg8e"
                },

                new HistoryModel
                {
                   Date="08",
                   Month="August",
                    Time="20:08",
                   Year="19",
                   CoinName="BTC",
                   CoinValue="20.202",
                   TypeImage="send",
                   Type="send",
                   Address="fg8f8fgfg8e"
                },

           };
        }
        public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);

        async Task InitialSetup(object data)
        {
            if (data != null)
            {
                if (data is CoinModel)
                {

                }
            }
            await Task.CompletedTask;
        }

        async Task ShowModules(object data)
        {
            switch (data.ToString())
            {
                case "Send":
                    {
                        SendView = true;
                        ReceiveView = false;
                        TransactionHistoryView = false;
                        ConvertView = false;
                        break;
                    }
                case "Receive":
                    {
                        SendView = false;
                        ReceiveView = true;
                        TransactionHistoryView = false;
                        ConvertView = false;
                        break;
                    }
                case "Convert":
                    {
                        SendView = false;
                        ReceiveView = false;
                        TransactionHistoryView = false;
                        ConvertView = true;
                        break;
                    }
                case "History":
                    {
                        SendView = false;
                        ReceiveView = false;
                        TransactionHistoryView = true;
                        ConvertView = false;
                        break;
                    }
            }
            await Task.CompletedTask;
        }

        async Task OnCopy()
        {
            await Clipboard.SetTextAsync(QrCode);
            ToastService.ShowToast("Copied");
        }

        async Task OnShare()
        {
            await Share.RequestAsync(new ShareTextRequest { Text = QrCode });
        }
    }
}
