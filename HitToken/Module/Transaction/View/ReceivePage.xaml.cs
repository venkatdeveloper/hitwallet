﻿using System;
using System.Collections.Generic;
using HitToken.Renderer;
using Xamarin.Forms;

namespace HitToken.Module.Root.View
{
    public partial class ReceivePage : CustomContentPage
    {
        public ReceivePage()
        {
            InitializeComponent();


            qrCodeImage.BarcodeOptions.Width = 300;
            qrCodeImage.BarcodeOptions.Height = 300;
        }
    }
}
