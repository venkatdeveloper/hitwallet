﻿using System;
using System.Collections.Generic;
using HitToken.Renderer;
using Xamarin.Forms;

namespace HitToken.Module.Transaction.View
{
    public partial class TransactionPage : CustomContentPage
    {
        public TransactionPage()
        {
            InitializeComponent();
            qrCodeImage.BarcodeOptions.Width = 300;
            qrCodeImage.BarcodeOptions.Height = 300;
        }
    }
}
