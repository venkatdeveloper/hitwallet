﻿using System;
namespace HitToken.Module.Notification.Model
{
    public class Notify

    {
        public string Title { get; set; }


        public string IpAddress { get; set; }

        public string ImageBg { get; set; }

        public string Date { get; set; }

        public string Month { get; set; }

        public string Year { get; set; }

        public string NewsTitle { get; set; }

        public string IpAdressDetails { get; set; }

        public string Readmore { get; set; }


    }
}
