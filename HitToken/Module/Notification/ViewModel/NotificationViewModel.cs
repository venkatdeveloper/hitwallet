﻿using System;
using System.Collections.Generic;
using HitToken.Module.Common;
using HitToken.Module.Notification.Model;

namespace HitToken.Module.Notification.ViewModel
{
    public class NotificationViewModel : ViewModelBase
    {


        
         


        List<Notify> notificationList;
        public List<Notify> NotificationList
        {
            get => notificationList;
            set
            {
                notificationList = value;
                OnPropertyChanged();
            }
        }

        public NotificationViewModel()
        {
            NotificationList = new List<Notify>
            {
                new Notify
                {
                    Title = "BITCOIN",
                    IpAddress = "Server IP address could not be found",
                    Date = "08",
                    Month="August",
                    Year="19",
                    NewsTitle="News Heading",
                    IpAdressDetails="www.google.com's server IP addresscould not be found",
                    ImageBg="dashboard_bg",
                    Readmore="readmore...."


                },
                new Notify
                {
                    Title = "HITPAY",
                    IpAddress = "address could not be found",
                    Date = "09",
                    Month="August",
                    Year="19",
                    NewsTitle="News Heading",
                     IpAdressDetails="www.google.com's server IP addresscould not be found",
                      ImageBg="notifybg",
                       Readmore="readmore...."

                }

            };

        }
    }
}
   







        