﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.ChangePassword.ViewModel;
using HitToken.Module.Common;
using HitToken.Module.MnemonicPhrase.ViewModel;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.Settings.ViewModel
{
    public class SettingsViewModel:ViewModelBase
    {
        public SettingsViewModel()
        {
        }

        public ICommand GotoChangePasswordCmd => new AsyncCommand(OnChangePassword);
        async Task OnChangePassword() => await NavigationService.NavigateToAsync<ChangePasswordViewModel>("Login");


        public ICommand GotoPaymentPasswordCmd => new AsyncCommand(OnPaymentPassword);
        async Task OnPaymentPassword() => await NavigationService.NavigateToAsync<ChangePasswordViewModel>("Payment");


        public ICommand GotoSetLanguageCmd => new AsyncCommand(OnSetLanguage);
        async Task OnSetLanguage() => await NavigationService.NavigateToAsync<SetLanguageViewModel>();


        public ICommand GotoGoogleAuth => new AsyncCommand(OnGoogleAuth);
        async Task OnGoogleAuth() => await NavigationService.NavigateToAsync<GoogleAuthenticatorViewModel>();


        public ICommand GotoMnemonicPhraseCmd => new AsyncCommand(OnMnemonicPhrase);
        async Task OnMnemonicPhrase() => await NavigationService.NavigateToAsync<MnemonicPhraseViewModel>();


        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

    }
}
