﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Utils;

namespace HitToken.Module.Referral.Model
{
    public class Level : ViewModelBase
    {
        public string ItemNo { get; set; }


        public string UserId { get; set; }



        public string Amount { get; set; }



        // public string CoinSymbol { get; set; }


        public string TeamAmount { get; set; }


        public string SelfAmount { get; set; }
        bool referralUsers;
        public bool ReferralUsers
        {
            get => referralUsers;
            set
            {
                referralUsers = value;
                OnPropertyChanged();
            }
        }
        public List<Level> LevelList { get; set; }

        public ICommand ShowReferralUser => new AsyncCommand(OnShowReferrals);

        async Task OnShowReferrals()
        {
            if (ReferralUsers)
                ReferralUsers = false;
            else
                ReferralUsers = true;
            await Task.CompletedTask;
        }
    }
}
