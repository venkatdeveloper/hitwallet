﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.Referral.ViewModel
{
    public class ReferralViewModel:ViewModelBase
    {
        public ReferralViewModel()
        {
        }


        public ICommand GotoLevelCmd => new AsyncCommand(OnLevel);
        async Task OnLevel() => await NavigationService.NavigateToAsync<LevelViewModel>();


        public ICommand GoToMatchingCmd => new AsyncCommand(OnMatching);
        async Task OnMatching() => await NavigationService.NavigateToAsync<MatchingViewModel>();

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();
    }
}
