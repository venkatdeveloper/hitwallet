﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Referral.Model;
using HitToken.Utils;

namespace HitToken.Module.Referral.ViewModel
{
    public class LevelViewModel : ViewModelBase
    {


        List<Level> levelList;
        public List<Level> LevelList
        {
            get => levelList;
            set
            {
                levelList = value;
                OnPropertyChanged();
            }
        }
        Level selectedItemLevel;
        public Level SelectedItemLevel
        {
            get => selectedItemLevel;
            set
            {
                selectedItemLevel = value;
                OnPropertyChanged();
                
                    _ = LevelSelected();
            }
        }

        string levelOneBackground = "#F6FF05";
        public string LevelOneBackground
        {
            get => levelOneBackground;
            set
            {
                levelOneBackground = value;
                OnPropertyChanged();
            }
        }
        string levelTwoBackground;
        public string LevelTwoBackground
        {
            get => levelTwoBackground;
            set
            {
                levelTwoBackground = value;
                OnPropertyChanged();
            }
        }
        bool leveOne = true;
        public bool LevelOne
        {
            get => leveOne;
            set
            {
                leveOne = value;
                OnPropertyChanged();
            }
        }

        bool levelTwo;
        public bool LevelTwo
        {
            get => levelTwo;
            set
            {
                levelTwo = value;
                OnPropertyChanged();
            }
        }

        public ICommand GotoLevel2Cmd => new AsyncCommand(OnLevel2);
        async Task OnLevel2() => await NavigationService.NavigateToAsync<Level2ViewModel>();

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

        public ICommand LevelOneClickedCmd => new AsyncCommand(LevelsChanged);



        public LevelViewModel()
        {
            LevelList = new List<Level>
            {
                new Level
                {
                    ItemNo="1",
                    UserId = "556",
                    Amount = "3030",
                    LevelList = new List<Level>
                    {
                        new Level
                        {
                            ItemNo="1",
                            UserId = "56",
                            Amount = "3030",
                        }
                    },

                },
                new Level
                {
                     ItemNo="2",
                     UserId = "56",
                    Amount = "3030",
                    LevelList = new List<Level>
                    {
                        new Level
                        {
                            ItemNo="1",
                            UserId = "56",
                            Amount = "3030",
                        }
                    },

                },
                new Level
                {
                     ItemNo="3",
                    UserId = "26",
                    Amount = "3030",
                    LevelList = new List<Level>
                    {
                        new Level
                        {
                            ItemNo="1",
                            UserId = "56",
                            Amount = "3030",
                        }
                    },

                },

                new Level
                {
                     ItemNo="4",
                     UserId = "526",
                    Amount = "3030",
                    LevelList = new List<Level>
                    {
                        new Level
                        {
                            ItemNo="1",
                            UserId = "56",
                            Amount = "3030",
                        }
                    },

                },

                new Level
                {
                     ItemNo="5",
                    UserId = "16",
                    Amount = "3030",
                    LevelList = new List<Level>
                    {
                        new Level
                        {
                            ItemNo="1",
                            UserId = "56",
                            Amount = "3030",
                        },
                        new Level
                        {
                            ItemNo="2",
                            UserId = "56",
                            Amount = "3030",
                        }
                    },

                },




           };

        }

        async Task LevelsChanged(object level)
        {
            switch (level.ToString())
            {
                case "1":
                    {
                        LevelOneBackground = "#F6FF05";
                        LevelTwoBackground = "white";
                        LevelOne = true;
                        LevelTwo = false;
                        break;
                    }
                case "2":
                    {
                        LevelOneBackground = "White";
                        LevelTwoBackground = "#F6FF05";
                        LevelOne = false;
                        LevelTwo = true;
                        break;
                    }
            }
            await Task.CompletedTask;
        }

        async Task LevelSelected()
        {
            foreach (var item in LevelList)
            {
                item.ReferralUsers = false;
            }
            SelectedItemLevel.ReferralUsers = true;
            OnPropertyChanged(nameof(LevelList));
            await Task.CompletedTask;
        }

    }
}



