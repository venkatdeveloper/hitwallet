﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Referral.Model;
using HitToken.Utils;

namespace HitToken.Module.Referral.ViewModel
{
    public class Level2ViewModel : ViewModelBase
    {
        List<Level> levelList;
        public List<Level> LevelList
        {
            get => levelList;
            set
            {
                levelList = value;
                OnPropertyChanged();
            }
        }

        public Level2ViewModel()
        {
            LevelList = new List<Level>
            {
                new Level
                {
                    ItemNo="1",
                    UserId = "556",
                    Amount = "3030",
                    TeamAmount="4646",
                    SelfAmount="75665"

                },
                new Level
                {
                   ItemNo="2",
                    UserId = "556",
                    Amount = "3030",
                    TeamAmount="6464",
                    SelfAmount="423"


                },
                new Level
                {
                   ItemNo="3",
                    UserId = "556",
                    Amount = "3030",
                    TeamAmount="32",
                    SelfAmount="23"

                },

                new Level
                {
                   ItemNo="4",
                    UserId = "556",
                    Amount = "3030",
                     TeamAmount="33",
                    SelfAmount="33"


                },

                new Level
                {
                   ItemNo="5",
                    UserId = "556",
                    Amount = "3030",
                     TeamAmount="323",
                    SelfAmount="433"


                },




           };

        }

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

    }
}



 