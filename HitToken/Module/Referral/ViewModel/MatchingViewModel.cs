﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.Referral.ViewModel
{
    public class MatchingViewModel:ViewModelBase
    {
        public MatchingViewModel()
        {
        }

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

    }
}
