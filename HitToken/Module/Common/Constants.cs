﻿namespace HitToken.Module.Common
{
    public sealed class Constants
    {
        Constants() { }

        internal const string APP_NAME = "MultiCoin";
        internal const string BASE_URL = "https://nextgenwallet.io/WalletAPI/";
        internal const string TranslateUrl = "http://mymemory.translated.net/api/";
        //Router


        //current Implementation api
        internal const string LOGIN_API = "MemberLogin1.asp";
        internal const string VERIFY_USER_ID_API = "checkusername.asp";
        internal const string REGISTER_API = "member_registration.asp";

        //registration api
        internal const string REGISTRATION_FIRST_STEP_API = "forgot_used_id.asp";
        internal const string REFERRAL_API = "verify_referral_code.asp";
        internal const string REGISTRATION_EMAIL_API = "check_verification_code.asp";
        internal const string REGISTRATION_VERIFY_API = "get_mnemonic_phrase_words.asp";
        internal const string REGISTRATION_LOGIN_PWD_API = "forgot_password.asp";
        internal const string REGISTRATION_PAYMENT_PWD_API = "forgot_password.asp";


        internal const string FORGOT_USERID_API = "forgot_used_id.asp";
        internal const string VERIFY_REFERRAL_CODE = "verify_referral_code.asp";
        internal const string CHECK_VERIFY_CODE = "check_verification_code.asp";
        internal const string GET_MNEMONIC_PHRASE_API = "get_mnemonic_phrase_words.asp";
        internal const string FORGOTPASSWORD_API = "forgot_password.asp";
        internal const string FORGOTPASSWORD_PHRASE_API = "forgot_password.asp";
        internal const string GET_DASHBOARD_DETAILS_API = "dashboard_details.asp";
        internal const string ACTIVATE_AI_PROJECT_API = "activate_ai_project.asp";
        internal const string GET_LEVEL_USERS_LIST_API = "get_level_one_users_list.asp";
        internal const string GET_MATCHING_USERS_API = "get_matching_users_list.asp";
        internal const string GET_PAYMENT_LIST_API = "get_payment_list.asp";
        internal const string GET_PROJECT_HISTORY_API = "get_Project_history.asp";
        internal const string CHANGE_PASSWORD_API = "change_password.asp";
        internal const string TRANSACTION_HISTORY_API = "transaction_history.asp";
        internal const string SEND_TOKEN_API = "send_token.asp";
        internal const string CONVERT_CURRENCY_API = "convert_currency.asp";
        internal const string CONVERT_CURRENCY_HISTORY_API = "convert_history.asp";
        internal const string NEW_CHANGE_REQUEST_API = "create_new_request.asp";
        internal const string ADD_CARD_DETAILS_API = "add_card_details.asp";
        internal const string RECHARGE_MY_WALLET = "recharge_my_wallet.asp";
        internal const string GET_NOTIFICATIONS_API = "get_notifications.asp";
        internal const string CREATE_TWO_FA_API = "create_2fa.asp";
        internal const string VERIFY_TWO_FA_API = "verify_2fa.asp";

        //Message
        internal const string OK = "Ok";
        internal const string CANCEL = "Cancel";
        internal const string LOGOUT = "Logout";
        internal const string SUCCESS = "Success";
        internal const string FAILURE = "Failure";
        internal const string DELETE = "Delete";
        internal const string ATTACHMENTS = "Attachments";
        internal const string CHECK_CONNECTIVITY = "Please check your internet connectivity";
        internal const string CONFIRMATION = "Confirmation";
        internal const string SIGN_OUT_CONFIRMATION_MESSAGE = "Are you sure you want to sign out of your account";
        internal const string INVALID_CERDENTIALS = "Invalid credentials";
        internal const string EMPTY_USERNAME = "Enter the user name";
        internal const string EMPTY_FULLNAME = "Enter the fullname";
        internal const string EMPTY_PASSWORD = "Enter the password";
        internal const string EMPTY_VERIFICATION_CODE = "Enter the verification code";
        internal const string PASSWORD_MISMATCH = "Password mismatch";
        internal const string EMPTY_EMAIL = "Enter the Email";
        internal const string EMPTY_REFERID = "Enter the Referid";
        internal const string INVALID_EMAIL = "Enter the valid Email";
        internal const string EMPTY_PHRASE = "Enter the phrase";
        internal const string EMPTY_OTP = "Please enter the otp";
        internal const string CONFIRM_PIN = "Enter Confirm Password";
        internal const string CREATE_PIN = "Create your 4 digit pin";
        internal const string ENTER_PIN = "Enter your 4 digit pin";
        internal const string ENTER_OLD_PASSWORD = "Enter your old Password";
        internal const string ENTER_NEW_PASSWORD = "Enter your new Password";
        internal const string ENTER_OLD_PIN = "Enter your old Pin";
        internal const string ENTER_NEW_PIN = "Enter your new Pin";
        internal const string TOKEN_EXPIRED = "Your token has expired. Please Login to continue";
        internal const string WARNING = "WARNING";
        internal const string TERMS = "1. The commision you receive from the referral program will initially be set at a rate of 20%. For accounts holding 500BNB or more, this rate will increase to 40%." + "\n\n" + "2.The fee commison will be sent instantly in real-time to your Binance account as your referee completes each trade and will be paid to you in whatever token/cryptocurrency the original fee was paid in." + "\n\n" + "3.There is no limit to the number of friends you can refer, although we do reserve the right or change the referral program rules at any time." + "\n\n" + "4.Each reference must be signedup through your referral link, QRCode or referral id." + "\n\n" + "5.Binance will check for duplicate or fake accounts and will not pay out referral bonuses on these accounts, Duplicate or shared finances will result in disqualification.";
        internal const string ENABLE_PERMISSION = "Enable Premission";
        internal const string PERMISSION_ENABLE_INFO = "Goto app settings and enable storage and camera permission";

        //Formatter String
        internal const string DD_MMMM_YYYY = "dd MMMM yyyy";
        internal const string CAMERA = "CAMERA";
        internal const string GALLERY = "GALLERY";

        //HTTP KEY
        internal const string AUTHORIZATION_KEY = "Authorization";

        internal const string HIDE_MASTER_KEY = "com.binanceexchange.hide_master_key";
        internal const string CHANGE_USER_NAME_KEY = "com.binanceexchange.change_user_name_key";

        internal const string AUTHENTICATION_TOKEN_END_POINT = "https://api.cognitive.microsoft.com/sts/v1.0";
        public static readonly string TextTranslatorEndpoint = "https://api.microsofttranslator.com/v2/http.svc/translate";
    }
}