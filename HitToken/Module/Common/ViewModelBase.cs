﻿using System.ComponentModel;
using System.Threading.Tasks;
using HitToken.Services.Navigation;
using HitToken.Services.Toast;
using Xamarin.Forms;
using System.Runtime.CompilerServices;

namespace HitToken.Module.Common
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected readonly INavigationService NavigationService;
        protected readonly IToastService ToastService;
      


        bool isBusy;
        public bool IsBusy
        {
            get => isBusy;
            set
            {
                if (isBusy != value)
                {
                    isBusy = value;
                    OnPropertyChanged(nameof(IsBusy));
                }
            }
        }

        

        
        protected ViewModelBase()
        {
            NavigationService = Locator.Instance.Resolve<INavigationService>();
            
            ToastService = DependencyService.Get<IToastService>();
            

        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public virtual Task InitializeAsync(object navigationData) => Task.FromResult(true);

        
    }
}