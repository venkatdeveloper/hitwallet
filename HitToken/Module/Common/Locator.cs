﻿using System;
using HitToken.Services.Dialog;
using HitToken.Services.Navigation;
using HitToken.Services.Request;
using HitToken.Module.Login.ViewModel;
using HitToken.Module.Register.ViewModel;
using Autofac;

using HitToken.Module.Phrase.ViewModel;
using HitToken.Module.ChangePassword.ViewModel;
using HitToken.Module.DashBoard.ViewModel;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Payment.ViewModel;
using HitToken.Module.PojectHistory.ViewModel;
using HitToken.Module.ProjectWallet.ViewModel;
using HitToken.Module.Referral.ViewModel;
using HitToken.Module.SideMenu.ViewModel;
using HitToken.Module.Settings.ViewModel;
using HitToken.Module.Root.ViewModel;
using HitToken.Module.Support.ViewModel;
using HitToken.Module.MultiConvert.ViewModel;
using HitToken.Module.MnemonicPhrase.ViewModel;
using HitToken.Services.Authendication;

namespace HitToken.Module.Common
{
    public class Locator
    {
        static Locator instance;
        public static Locator Instance => instance ?? (instance = new Locator());

        readonly IContainer builder;

        Locator()
        {
            var containerBuilder = new ContainerBuilder();

            //Register Services
            containerBuilder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            containerBuilder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();
            containerBuilder.RegisterType<RequestService>().As<IRequestService>().SingleInstance();
            containerBuilder.RegisterType<AuthendicationServices>().As<IAuthendicationServices>().SingleInstance();
            //containerBuilder.RegisterType<PinService>().As<IPinService>().SingleInstance();
            //containerBuilder.RegisterType<TransactionServices>().As<ITransactionServices>().SingleInstance();
            //containerBuilder.RegisterType<AccountServices>().As<IAccountServices>().SingleInstance();


            //Register ViewModels
            //containerBuilder.RegisterType<QrCodePopupViewModel>();
            //containerBuilder.RegisterType<FeeViewModel>();
            //containerBuilder.RegisterType<SecurityViewModel>();
            //containerBuilder.RegisterType<ReferralViewModel>();
            containerBuilder.RegisterType<RegisterViewModel>();
            //containerBuilder.RegisterType<ChangePasswordViewModel>();
            //containerBuilder.RegisterType<WithdrawViewModel>();
            containerBuilder.RegisterType<PhraseViewModel>();
            containerBuilder.RegisterType<LoginViewModel>();
            containerBuilder.RegisterType<ForgotPasswordViewModel>();
            containerBuilder.RegisterType<ForgotUserIdViewModel>();


            containerBuilder.RegisterType<ChangePasswordViewModel>();
            containerBuilder.RegisterType<GoogleAuthenticatorViewModel>();
            containerBuilder.RegisterType<SetLanguageViewModel>();
            containerBuilder.RegisterType<DashBoardViewModel>();
            containerBuilder.RegisterType<CardViewModel>();
            containerBuilder.RegisterType<CardDetailsViewModel>();

            containerBuilder.RegisterType<NotificationViewModel>();
            containerBuilder.RegisterType<PaymentViewModel>();
            containerBuilder.RegisterType<ProjectHistoryViewModel>();
            containerBuilder.RegisterType<ProjectWalletViewModel>();


            containerBuilder.RegisterType<PopupViewModel>();
            containerBuilder.RegisterType<LevelViewModel>();
            containerBuilder.RegisterType<Level2ViewModel>();
            containerBuilder.RegisterType<MatchingViewModel>();


            containerBuilder.RegisterType<ReferralViewModel>();
            containerBuilder.RegisterType<SettingsViewModel>();
            containerBuilder.RegisterType<RootViewModel>();
            containerBuilder.RegisterType<SideMenuViewModel>();

            containerBuilder.RegisterType<SupportViewModel>();
            containerBuilder.RegisterType<ConvertHistoryViewModel>();
            containerBuilder.RegisterType<ConvertViewModel>();
            containerBuilder.RegisterType<ReceiveViewModel>();

            containerBuilder.RegisterType<SendViewModel>();
            containerBuilder.RegisterType<TransactionHistoryViewModel>();
            containerBuilder.RegisterType<TransactionViewModel>();


            containerBuilder.RegisterType<CardSidesViewModel>();
            containerBuilder.RegisterType<MultiConvertViewModel>();
            containerBuilder.RegisterType<MnemonicPhraseViewModel>();

            //containerBuilder.RegisterType<RootViewModel>();
            //containerBuilder.RegisterType<ProfileViewModel>();
            //containerBuilder.RegisterType<HomeViewModel>();
            //containerBuilder.RegisterType<MarketViewModel>();
            //containerBuilder.RegisterType<TradeViewModel>();
            //containerBuilder.RegisterType<TransactionHistoryViewModel>();
            //containerBuilder.RegisterType<WalletViewModel>();
            //containerBuilder.RegisterType<OrderViewModel>();
            //containerBuilder.RegisterType<OrderHistoryViewModel>();
            //containerBuilder.RegisterType<CoinPairViewModel>();
            //containerBuilder.RegisterType<SupportViewModel>();
            //containerBuilder.RegisterType<ChangeRequestDetailViewModel>();
            //containerBuilder.RegisterType<SupportHistoryViewModel>();
            //containerBuilder.RegisterType<ReplyPopupViewModel>();



            builder = containerBuilder.Build();
        }

        public T Resolve<T>() => builder.Resolve<T>();

        public object Resolve(Type type) => builder.Resolve(type);
    }
}
