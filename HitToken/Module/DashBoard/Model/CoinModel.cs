﻿using System;
using HitToken.Utils;
using System.Windows.Input;
using HitToken.Services.Navigation;
using HitToken.Module.ProjectWallet.ViewModel;

namespace HitToken.Module.DashBoard.Model
{
    public class CoinModel
    {

        public string CoinName { get; set; }


        public string CoinLogo { get; set; }



       // public string CoinSymbol { get; set; }


        public string WalletBalance { get; set; }


        public string MarketRatePercentage { get; set; }

        public string Time { get; set; }

        public string EquivalentDollar { get; set; }

        public string EquivalentAvailableDollar { get; set; }

       // public ICommand PopupCmd => new AsyncCommand(() => NavigationService.NavigateToAsync<PopupViewModel>());
    }
}
