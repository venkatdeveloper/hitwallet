﻿using System;
using HitToken.Module.Common;

namespace HitToken.Module.DashBoard.ViewModel
{
    public class CardDetailsViewModel:ViewModelBase
    {
        public CardDetailsViewModel()
        {
        }


        string AvailableAmount="200.00";
        public string availableAmount
        {
            get => AvailableAmount;
            set
            {
                AvailableAmount = value;
                OnPropertyChanged();
            }
        }


        string hitpayUSDT="0.525";
        public string HitpayUSDT
        {
            get => hitpayUSDT;
            set
            {
                hitpayUSDT = value;
                OnPropertyChanged();
            }
        }

        string usdtValue="200";
        public string USDTValue
        {
            get => usdtValue;
            set
            {
                usdtValue = value;
                OnPropertyChanged();
            }
        }


        string cardFee="50.00";
        public string CardFee
        {
            get => cardFee;
            set
            {
                cardFee = value;
                OnPropertyChanged();
            }
        }


        string cardRecharge="20.00";
        public string CardRecharge
        {
            get => cardRecharge;
            set
            {
                cardRecharge = value;
                OnPropertyChanged();
            }
        }


        string totalAmountUSDT="250.00";
        public string TotalAmountUSDT
        {
            get => totalAmountUSDT;
            set
            {
                totalAmountUSDT = value;
                OnPropertyChanged();
            }
        }

        string totalAmountHitpay="131.32";
        public string TotalAmountHitpay
        {
            get => totalAmountHitpay;
            set
            {
                totalAmountHitpay = value;
                OnPropertyChanged();
            }
        }


    }
}
