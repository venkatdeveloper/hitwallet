﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.DashBoard.Model;
using HitToken.Module.MultiConvert.ViewModel;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Payment.ViewModel;
using HitToken.Module.PojectHistory.ViewModel;
using HitToken.Module.ProjectWallet.ViewModel;
using HitToken.Module.Referral.ViewModel;
using HitToken.Module.Root.ViewModel;
using HitToken.Module.Settings.ViewModel;
using HitToken.Utils;

namespace HitToken.Module.DashBoard.ViewModel
{
    public class DashBoardViewModel : ViewModelBase
    {

        ObservableCollection<CoinModel> coinsList;
        public ObservableCollection<CoinModel> CoinsList
        {
            get => coinsList;
            set
            {
                coinsList = value;
                OnPropertyChanged();
            }
        }

      
        CoinModel selectedCoin;
        public CoinModel SelectedCoin
        {
            get => selectedCoin;
            set
            {
                selectedCoin = value;
                OnPropertyChanged();
                _ = OnCoinSelected();
            }
        }

        public ICommand GotoProjectCmd => new AsyncCommand(OnProject);
        async Task OnProject() => await NavigationService.NavigateToAsync<ProjectWalletViewModel>();

        public ICommand GotoReferalCmd => new AsyncCommand(OnReferal);
        async Task OnReferal() => await NavigationService.NavigateToAsync<ReferralViewModel>();

        public ICommand GotoPaymentCmd => new AsyncCommand(OnPayment);
        async Task OnPayment() => await NavigationService.NavigateToAsync<PaymentViewModel>();


        public ICommand GotoProjectHistory => new AsyncCommand(OnProjectHistory);
        async Task OnProjectHistory() => await NavigationService.NavigateToAsync<ProjectHistoryViewModel>();


        public ICommand GotoSettingsCmd => new AsyncCommand(OnSettings);
        async Task OnSettings() => await NavigationService.NavigateToAsync<SettingsViewModel>();


        public ICommand GotoMultiConvertCmd => new AsyncCommand(OnMultiConvert);
        async Task OnMultiConvert() => await NavigationService.NavigateToAsync<MultiConvertViewModel>();

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();

        

        string referralCode="2367263";
        public string ReferralCode
        {
            get => referralCode;
            set
            {
                referralCode = value;
                OnPropertyChanged();
            }
        }
        string position = "Left";
        public string Position
        {
            get => position;
            set
            {
                position = value;
                OnPropertyChanged();
            }
        }

        public DashBoardViewModel()
        {

            CoinsList = new ObservableCollection<CoinModel>
           {
               new CoinModel
               {
                   CoinName="HitPay",
                   CoinLogo="app_logo",
                   WalletBalance="0.4646",
                   MarketRatePercentage="0.25",
                   Time="20:04",
                   EquivalentDollar="0.376",
                   EquivalentAvailableDollar="0.0404",
               },
               new CoinModel
               {
                   CoinName="BTC",
                   CoinLogo="bitcoin",
                   WalletBalance="0.54",
                   MarketRatePercentage="0.64",
                   Time="20:06",
                   EquivalentDollar="0.56",
                   EquivalentAvailableDollar="0.995",
               },
               new CoinModel
               {
                  CoinName="ETH",
                   CoinLogo="etherium",
                   WalletBalance="0.565",
                   MarketRatePercentage="0.5",
                   Time="10:50",
                   EquivalentDollar="0.4",
                   EquivalentAvailableDollar="24.5",

               },

                new CoinModel
               {
                    CoinName="BCH",
                   CoinLogo="bitcoincash",
                   WalletBalance="0.44",
                   MarketRatePercentage="0.3",
                   Time="06:04",
                   EquivalentDollar="0.4",
                   EquivalentAvailableDollar="45.5",

               },

                 new CoinModel
               {
                   CoinName="LTC",
                   CoinLogo="ltc",
                   WalletBalance="0.42",
                   MarketRatePercentage="0.22",
                   Time="05:02",
                   EquivalentDollar="0.33",
                   EquivalentAvailableDollar="34.67",

               },

                   new CoinModel
               {
                   CoinName="USDT",
                   CoinLogo="usdt",
                   WalletBalance="0.42",
                   MarketRatePercentage="0.22",
                   Time="05:02",
                   EquivalentDollar="0.33",
                   EquivalentAvailableDollar="34.67",

               },

           };

        }
        async Task OnCoinSelected()
        {
            await NavigationService.NavigateToAsync<TransactionViewModel>(SelectedCoin);
        }

    }


    //async Task GetDashboardDetail()
    //{
    //    //if (!NetworkUtils.IsConnected)
    //    //{
    //    //    ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
    //    //    return;
    //    //}
    //    //IsBusy = true;
    //    //var response = await accountService.DashboardDetailsAsync();
    //    //IsBusy = false;
    //    //if (!response.IsSuccess())
    //    //{
    //    //    ToastService.ShowToast(response.Message);
    //    //    return;
    //    //}
    //    dashboardData = response;
    //    SetData();
    //}

    //public void SetData()
    //{
    //    if (dashboardData != null)
    //    {
    //        //USDBalance = dashboardData.USDBalance;
    //        //XRPBalance = dashboardData.WithdrawBalance;
    //        //XRPCurrentRate = dashboardData.CoinRate;
    //        //LendingBalance = dashboardData.LendingBalance;
    //        //TotalLedgerBalance = dashboardData.TotalLedgerAmount;
    //        //AppSettings.AffiliateLink = dashboardData.AffiliateLink;
    //    }
    //}
}
