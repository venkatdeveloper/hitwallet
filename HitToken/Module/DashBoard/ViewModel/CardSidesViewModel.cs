﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Utils;

namespace HitToken.Module.DashBoard.ViewModel
{
    public class CardSidesViewModel:ViewModelBase
    {
        

        bool backSide;
        public bool BackSide
        {
            get => backSide;
            set
            {
                backSide = value;
                OnPropertyChanged();
            }
        }



        bool frontSide;
        public bool FrontSide
        {
            get => frontSide;
            set
            {
                frontSide = value;
                OnPropertyChanged();
            }
        }


        public CardSidesViewModel()
        {
            BackSide = true;

        }

        public ICommand BackSideCmd => new AsyncCommand(OnBackSide);

        async Task OnBackSide()
        {

            BackSide = false;
            FrontSide = true;

            await Task.CompletedTask;

        }

    }
}
