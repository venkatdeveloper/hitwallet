﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Phrase.Model;
using HitToken.Utils;

namespace HitToken.Module.Phrase.ViewModel
{
    public class PhraseViewModel : ViewModelBase
    {
        public ICommand NextCmd => new AsyncCommand(OnNext);
        public ICommand CopyCmd => new AsyncCommand(OnCopy);
        public ICommand DownloadCmd => new AsyncCommand(OnDownload);
        public ICommand LoginCmd => new AsyncCommand(() => NavigationService.NavigateBackAsync());
        public ICommand DoneCmd => new AsyncCommand(OnGreetings);

        ObservableCollection<PhraseModel> selectedPhraseList = new ObservableCollection<PhraseModel>();
        public ObservableCollection<PhraseModel> SelectedPhraseList
        {
            get => selectedPhraseList;
            set
            {
                selectedPhraseList = value;
                OnPropertyChanged();
            }
        }
        PhraseModel selectedPhraseForRemove;
        public PhraseModel SelectedPhraseForRemove
        {
            get => selectedPhraseForRemove;
            set
            {
                selectedPhraseForRemove = value;
                OnPropertyChanged();
                AddAndRemovePhraseList();
            }
        }
        ObservableCollection<PhraseModel> backUpPhraseList;
        public ObservableCollection<PhraseModel> BackUpPhraseList
        {
            get => backUpPhraseList;
            set
            {
                backUpPhraseList = value;
                OnPropertyChanged();
            }
        }
        ObservableCollection<PhraseModel> phraseList;
        public ObservableCollection<PhraseModel> PhraseList
        {
            get => phraseList;
            set
            {
                phraseList = value;
                OnPropertyChanged();
            }
        }
        PhraseModel selectedPhrase;
        public PhraseModel SelectedPhrase
        {
            get => selectedPhrase;
            set
            {
                selectedPhrase = value;
                OnPropertyChanged();
                AddToSelectedPhraseList();
            }
        }
        bool phraseStartStack = true;
        public bool PhraseStartStack
        {
            get => phraseStartStack;
            set
            {
                phraseStartStack = value;
                OnPropertyChanged();
            }
        }
        bool phraseSelectStack;
        public bool PhraseSelectStack
        {
            get => phraseSelectStack;
            set
            {
                phraseSelectStack = value;
                OnPropertyChanged();
            }
        }
        bool greetingsStack;
        public bool GreetingsStack
        {
            get => greetingsStack;
            set
            {
                greetingsStack = value;
                OnPropertyChanged();
            }
        }

        public PhraseViewModel()
        {
            PhraseList = new ObservableCollection<PhraseModel>
            {
                new PhraseModel
                {
                    PhraseText = "industry"
                },
                new PhraseModel
                {
                    PhraseText = "text"
                },
                new PhraseModel
                {
                    PhraseText = "Lorem"
                },
                new PhraseModel
                {
                    PhraseText = "dummy"
                },
                new PhraseModel
                {
                    PhraseText = "simply"
                },
                new PhraseModel
                {
                    PhraseText = "printing"
                },
                new PhraseModel
                {
                    PhraseText = "typetesting"
                },
                new PhraseModel
                {
                    PhraseText = "ipsum"
                },

            };
            BackUpPhraseList = new ObservableCollection<PhraseModel>
            {
                new PhraseModel
                {
                    PhraseText = "Lorem"
                },
                new PhraseModel
                {
                    PhraseText = "ipsum"
                },
                new PhraseModel
                {
                    PhraseText = "simply"
                },
                new PhraseModel
                {
                    PhraseText = "dummy"
                },
                new PhraseModel
                {
                    PhraseText = "text"
                },
                new PhraseModel
                {
                    PhraseText = "printing"
                },
                new PhraseModel
                {
                    PhraseText = "typetesting"
                },
                new PhraseModel
                {
                    PhraseText = "industry"
                },

            };
        }
        async Task OnNext()
        {
            PhraseStartStack = false;
            PhraseSelectStack = true;
            await Task.CompletedTask;
        }

        async Task OnCopy()
        {
            ToastService.ShowToast("Copied");
            await Task.CompletedTask;
        }
        async Task OnDownload()
        {
            ToastService.ShowToast("Downloaded");
            await Task.CompletedTask;
        }
        async Task OnGreetings()
        {
            for (int i = 0; i < BackUpPhraseList.Count; i++)
            {
                if (BackUpPhraseList[i].PhraseText != SelectedPhraseList[i].PhraseText)
                {
                    ToastService.ShowToast($"Incorrect phrase selection in position: {i + 1} ");
                    return;
                }
            }
            PhraseSelectStack = false;
            GreetingsStack = true;
            await Task.CompletedTask;
        }
        void AddToSelectedPhraseList()
        {
            SelectedPhraseList.Add(SelectedPhrase);
            PhraseList.Remove(SelectedPhrase);
        }
        void AddAndRemovePhraseList()
        {
            PhraseList.Add(SelectedPhraseForRemove);
            SelectedPhraseList.Remove(SelectedPhraseForRemove);
        }
    }
}
