﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.DashBoard.Model;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.ProjectWallet.Model;
using HitToken.Utils;

namespace HitToken.Module.ProjectWallet.ViewModel
{
    public class ProjectWalletViewModel : ViewModelBase
    {


        List<CoinModel> coinsList;
        public List<CoinModel> CoinsList
        {
            get => coinsList;
            set
            {
                coinsList = value;
                OnPropertyChanged();
            }
        }


        bool hitman;
        public bool Hitman
        {
            get => hitman;
            set
            {
                hitman = value;
                OnPropertyChanged();
            }
        }


        bool condition;
        public bool Condition
        {
            get => condition;
            set
            {
                condition = value;
                OnPropertyChanged();
            }
        }



        bool projectPage;
        public bool ProjectPage
        {
            get => projectPage;
            set
            {
                projectPage = value;
                OnPropertyChanged();
            }
        }



        CoinModel selectedItem;
        public CoinModel SelectedItem
        {
            get => selectedItem;
            set
            {
                selectedItem = value;
                OnPropertyChanged();
                if(SelectedItem != null)
                   _ = OnSelectedItem();
            }
        }



        async Task OnSelectedItem() => await NavigationService.NavigatePopupAsync<PopupViewModel>(SelectedItem);

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();


        public ICommand NextCmd => new AsyncCommand(OnNext);
        public ICommand AcceptandContinueCmd => new AsyncCommand(OnAcceptandContinue);

        public ProjectWalletViewModel()
        {
            CoinsList = new List<CoinModel>
           {
               new CoinModel
               {
                   CoinName="BTC",WalletBalance ="11,420.00",CoinLogo="bitcoin",EquivalentDollar="0.30"


               },
               new CoinModel
               {
                   CoinName="ETH",WalletBalance ="12,420.00",CoinLogo="etherium",EquivalentDollar="0.30"
               },

                 new CoinModel
               {
                   CoinName="BCH",WalletBalance ="18,420.00",CoinLogo="bitcoincash",EquivalentDollar="0.30"

               },
               new CoinModel
               {
                   CoinName="LTC",WalletBalance ="17,420.00",CoinLogo="ltc",EquivalentDollar="0.30"

               },



                 new CoinModel
               {
                   CoinName="USDT",WalletBalance="19,420.00",CoinLogo="usdt",EquivalentDollar="0.30"

               },



        };
            Hitman = true;

        }

        async Task OnNext()
        {

            Hitman = false;
            Condition = true;

            await Task.CompletedTask;

        }

        async Task OnAcceptandContinue()
        {

            Condition = false;
            ProjectPage = true;

            await Task.CompletedTask;

        }
    }
}
