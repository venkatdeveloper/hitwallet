﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.DashBoard.Model;
using HitToken.Utils;
using Rg.Plugins.Popup.Services;

namespace HitToken.Module.ProjectWallet.ViewModel
{
    public class PopupViewModel : ViewModelBase
    {




        bool popup = true;
        public bool Popup
        {
            get => popup;
            set
            {
                popup = value;
                OnPropertyChanged();
            }
        }




        bool success;
        public bool Success
        {
            get => success;
            set
            {
                success = value;
                OnPropertyChanged();
            }
        }
        string coinName;
        public string CoinName
        {
            get => coinName;
            set
            {
                coinName = value;
                OnPropertyChanged();
            }
        }

        string fees;
        public string Fees
        {
            get => fees;
            set
            {
                fees = value;
                OnPropertyChanged();
            }
        }

        string coinLogo;
        public string CoinLogo
        {
            get => coinLogo;
            set
            {
                coinLogo = value;
                OnPropertyChanged();
            }
        }

        string coinBalance;
        public string CoinBalance
        {
            get => coinBalance;
            set
            {
                coinBalance = value;
                OnPropertyChanged();
            }
        }

        public ICommand ProceedCmd => new AsyncCommand(OnProceed);

        public ICommand CloseCmd => new AsyncCommand(OnClose);

        async Task OnClose() => await PopupNavigation.Instance.PopAllAsync();


        public PopupViewModel()
        {
          
        }
        public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);

        async Task InitialSetup(object data)
        {
            if(data != null)
            {
                if(data is CoinModel coin)
                {
                    CoinBalance = coin.WalletBalance;
                    CoinLogo = coin.CoinLogo;
                    CoinName = coin.CoinName;
                    fees = coin.EquivalentDollar;
                }
            }
            await Task.CompletedTask;
        }

        async Task OnProceed()
        {

            Popup = false;
            Success = true;

            await Task.CompletedTask;


        }
    }
}
