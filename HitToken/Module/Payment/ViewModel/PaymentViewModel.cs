﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using HitToken.Module.Common;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.Transaction.Model;
using HitToken.Utils;

namespace HitToken.Module.Payment.ViewModel
{
    public class PaymentViewModel:ViewModelBase
    {
        List<HistoryModel> historyList;
        public List<HistoryModel> HistoryList
        {
            get => historyList;
            set
            {
                historyList = value;
                OnPropertyChanged();
            }
        }

        public PaymentViewModel()
        {
            HistoryList = new List<HistoryModel>
            {
                new HistoryModel
                {
                   Date="08",
                   Month="August",
                   Year="19",
                   Time="20:08",
                   CoinName="BTC",
                   CoinValue="20.202",

                   Type="My Profit Share",
                    TypeImage="send",
                   Address="fg8f8fgfg8e"


                },
                new HistoryModel
                {
                   Date="08",
                    Time="20:08",
                   Month="August",
                   Year="19",
                   CoinName="BTC",
                   CoinValue="20.202",
                   TypeImage="recieve",
                 Type="My Profit Share",
                   Address="fg8f8fgfg8e"



                },
                new HistoryModel
                {
                   Date="08",
                    Time="20:08",
                   Month="August",
                   Year="19",
                   CoinName="BTC",
                   CoinValue="20.202",
                    TypeImage="send",
                Type="My Profit Share",
                   Address="fg8f8fgfg8e"



                },

                new HistoryModel
                {
                  Date="08",
                   Month="August",
                   Year="19",
                    Time="20:08",
                   CoinName="BTC",
                   CoinValue="20.202",
                  TypeImage="recieve",
                 Type="My Profit Share",
                   Address="fg8f8fgfg8e"



                },

                new HistoryModel
                {
           Date="08",
                   Month="August",
                    Time="20:08",
                   Year="19",
                   CoinName="BTC",
                   CoinValue="20.202",
                   TypeImage="send",
                Type="My Profit Share",
                   Address="fg8f8fgfg8e"


                },




           };

        }

        public ICommand NotifyCmd => new AsyncCommand(OnNotify);
        async Task OnNotify() => await NavigationService.NavigateToAsync<NotificationViewModel>();
    }
}
