﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class PaymentsResponse : ResponseBase
    {
        [JsonProperty("data")]
        public List<Payments> Coins { get; set; }
    }

    public class Payments
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("profit_share_type")]
        public string ShareType { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("to_currency_type")]
        public string CurrencyType { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

    }
}
