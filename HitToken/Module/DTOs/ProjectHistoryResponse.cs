﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class ProjectHistoryResponse : ResponseBase
    {
        [JsonProperty("project_history")]
        public List<Payments> HistoryItems { get; set; }
    }
}
