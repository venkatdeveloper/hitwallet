﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class TransactionHistoryResponse : ResponseBase
    {
        [JsonProperty("data")]
        public List<Payments> HistoryItems { get; set; }

        [JsonProperty("available_wallet_balance")]
        public string AvailableWalletBalance { get; set; }
    }
}
