﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class MnemonicResponse : ResponseBase
    {
        [JsonProperty("phrase")]
        public List<string> Phrase { get; set; }
    }
}
