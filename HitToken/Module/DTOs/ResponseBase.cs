﻿using System;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class ResponseBase
    {
       [JsonProperty("status")]
       public string Status { get; set; }

        [JsonProperty("status_code")]
        public int StatusCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
