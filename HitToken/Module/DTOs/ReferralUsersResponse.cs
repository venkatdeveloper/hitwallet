﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class ReferralUsersResponse : ResponseBase
    {
        [JsonProperty("total_amount")]
        public string TotalAmount { get; set; }

        [JsonProperty("users_list")]
        public List<UsersDetails> Users { get; set; }
    }

    public class UsersDetails
    {
        [JsonProperty("user_id")]
        public string UserID { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("self_amount")]
        public string Self_Amount { get; set; }

        [JsonProperty("team_amount")]
        public string TeamAmount { get; set; }

        [JsonProperty("referral_users")]
        public List<UsersDetails> ReferalUsers { get; set; }

    }
}
