﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class DashboardResponse : ResponseBase
    {
        [JsonProperty("my_referral_id")]
        public string ReferralID { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }

        [JsonProperty("coin_details")]
        public List<CoinDetails> Coins { get; set; }

    }

    public class CoinDetails
    {
        [JsonProperty("coin_logo")]
        public string Logo { get; set; }

        [JsonProperty("coin_symbol")]
        public string Symbol { get; set; }

        [JsonProperty("coin_name")]
        public string CoinName { get; set; }

        [JsonProperty("updated_time")]
        public string UpdateTime { get; set; }

        [JsonProperty("available_balance")]
        public string AvailableBalance { get; set; }

        [JsonProperty("fee_percentage")]
        public string FeePercentage { get; set; }

        [JsonProperty("coin_address")]
        public string CoinAddress { get; set; }

        [JsonProperty("equivalent_dollar")]
        public string EquivalentDollar { get; set; }

        [JsonProperty("available_equivalent_dollar")]
        public string AvailableEquivalentDollar { get; set; }
    }
}
