﻿using System;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class LoginResponse : ResponseBase
    {
        [JsonProperty("token")]
        public string Token { get; set; }       
    }
}
