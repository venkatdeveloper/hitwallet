﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class NotificationResponse : ResponseBase
    {
        [JsonProperty("data")]
        public List<NotificationItems> Notifications { get; set; }
    }

    public class NotificationItems
    {
        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("dateandtime")]
        public string DateAndTime { get; set; }

        
    }
}
