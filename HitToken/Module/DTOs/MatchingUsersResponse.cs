﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace HitToken.Module.DTOs
{
    public class MatchingUsersResponse : ResponseBase
    {
        [JsonProperty("users_list")]
        public List<UsersDetails> Users { get; set; }
    }

    public class TeamDetails
    {
        [JsonProperty("total_users")]
        public string UsersCount { get; set; }

        [JsonProperty("team_name")]
        public string TeamName { get; set; }

        [JsonProperty("total_amount")]
        public string TotalAmount { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }
}
