﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HitToken.Module.Common;
using Xamarin.Forms;
using System.Linq;
using HitToken.Module.Login.ViewModel;
using HitToken.Module.Login.View;
using HitToken.Module.Register.ViewModel;
using HitToken.Module.Register.View;
using HitToken.Module.Phrase.ViewModel;
using HitToken.Module.Phrase.View;
using HitToken.Module.ChangePassword.ViewModel;
using HitToken.Module.ChangePassword.View;
using HitToken.Module.Referral.ViewModel;
using HitToken.Module.Referral.View;
using HitToken.Module.Root.ViewModel;
using HitToken.Module.DashBoard.ViewModel;
using HitToken.Module.Payment.ViewModel;
using HitToken.Module.DashBoard.View;
using HitToken.Module.Notification.View;
using HitToken.Module.PojectHistory.ViewModel;
using HitToken.Module.Notification.ViewModel;
using HitToken.Module.ProjectWallet.ViewModel;
using HitToken.Module.ProjectWallet.View;
using HitToken.Module.Root.View;
using HitToken.Module.Settings.ViewModel;
using HitToken.Module.Settings.View;
using HitToken.Module.SideMenu.View;
using HitToken.Module.Support.ViewModel;
using HitToken.Module.Support.View;
using HitToken.Module.SideMenu.ViewModel;
using HitToken.Module.Payment.View;
using HitToken.Module.PojectHistory.View;
using HitToken.Module.Transaction.View;
using HitToken.Module.MultiConvert.ViewModel;
using HitToken.Module.MultiConvert.View;
using HitToken.Module.MnemonicPhrase.ViewModel;
using HitToken.Module.MnemonicPhrase.View;
using Rg.Plugins.Popup.Services;
using Rg.Plugins.Popup.Pages;

namespace HitToken.Services.Navigation
{
    public class NavigationService : INavigationService
    {
        protected readonly Dictionary<Type, Type> mapping;

        public ViewModelBase PreviousPageViewModel
        {
            get
            {
                Page page = null;
                var nav = Application.Current.MainPage.Navigation.NavigationStack;
                //if (PopupNavigation.Instance.PopupStack.Count > 0)
                //    page = nav.Last();
                //else
                    page = nav.ElementAtOrDefault(nav.Count - 2);
                return page?.BindingContext as ViewModelBase;
            }
        }

        public NavigationService()
        {
            mapping = new Dictionary<Type, Type>();
            CreatePageViewModelMapping();
        }

        public Task InitializeAsync()
        {
            return NavigateToAsync<LoginViewModel>();
            //AppSettings.IsLogin? NavigateToAsync<ForgotPasswordViewModel>() :
        }

        //public Task InitializeNotificatioAsync(Dictionary<string, string> data)
        //{
        //    if (!data.Keys.Equals(Constants.OK))
        //    {
        //        NotificationItems notificationItems = new NotificationItems();
        //        foreach (var key in data)
        //        {
        //            notificationItems.Title = key.Key;
        //            notificationItems.Message = key.Value;
        //        }
        //        return NavigateToAsync<NotificationViewModel>(notificationItems);
        //    }
        //    else
        //        return AppSettings.isLogin ? NavigateToAsync<DashboardViewModel>() : NavigateToAsync<StartUpViewModel>();
        //}

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase => InternalNavigateToAsync(typeof(TViewModel));

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase => InternalNavigateToAsync(typeof(TViewModel), parameter);

        public Task NavigateToAsync(Type viewModelType, bool isMenuType = false) => InternalNavigateToAsync(viewModelType, isMenuType: isMenuType);

        public Task NavigateToAsync(Type viewModelType, object parameter, bool isMenuType = false) => InternalNavigateToAsync(viewModelType, parameter, isMenuType);

        public Task NavigateModelAsync<TViewModel>() where TViewModel : ViewModelBase => InternalNavigateModelAsync(typeof(TViewModel));

        public Task NavigateModelAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase => InternalNavigateModelAsync(typeof(TViewModel), parameter);

        public Task NavigatePopupAsync<TViewModel>(bool animate = true) where TViewModel : ViewModelBase => NavigatePopupAsync<TViewModel>(null, animate);

        public Task NavigatePopupAsync<TViewModel>(object parameter, bool animate = true) where TViewModel : ViewModelBase
        {
            var page = CreateAndBindPage(typeof(TViewModel));
            if (page is PopupPage popupPage)
            {
                PopupNavigation.Instance.PushAsync(popupPage, animate);
            }
            return (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }

        protected virtual Task InternalNavigateToAsync(Type viewModelType, object navigationData = null, bool isMenuType = false)
        {
            var page = CreateAndBindPage(viewModelType);
            if (page is LoginPage)
            {
                var nav = new LoginNavigationPage(page);
                
                Application.Current.MainPage = nav;
            }
            else if (page is RootPage)
            {
                Application.Current.MainPage = page;
            }
            else if (Application.Current.MainPage is RootPage rootPage)
            {
                rootPage.IsPresented = false;
                if (rootPage.Detail is NavigationPage navigationPage && !isMenuType)
                {
                    if (navigationPage.CurrentPage.GetType() != page.GetType())
                    {
                        navigationPage.PushAsync(page);
                    }
                }
                else
                {
                    var nav = new NavigationPage(page);
                    rootPage.Detail = nav;
                }
            }
            else if (Application.Current.MainPage is LoginNavigationPage loginNavigationPage)
            {
                loginNavigationPage.Navigation.PushAsync(page);
            }
            return (page.BindingContext as ViewModelBase).InitializeAsync(navigationData);
        }

        protected virtual Task InternalNavigateModelAsync(Type viewModelType, object navigationData = null)
        {
            var page = CreateAndBindPage(viewModelType);

            Application.Current.MainPage.Navigation.PushModalAsync(page);

            return (page.BindingContext as ViewModelBase).InitializeAsync(navigationData);
        }

        public Task RemoveBackStackAsync()
        {
            throw new NotImplementedException();
        }

        public Task NavigateBackAsync() => InternalBackNavigation();

        public Task NavigateBackModelAsync() => InternalBackNavigation(true);

        protected virtual Task InternalBackNavigation(bool isModelPage = false)
        {
            if (Application.Current.MainPage is RootPage rootPage)
            {
                if (rootPage.Detail is NavigationPage navigation)
                {
                    if (isModelPage)
                        return navigation.Navigation.PopModalAsync();
                    else
                        return navigation.Navigation.PopAsync();
                }
                else
                {
                    if (isModelPage)
                        return rootPage.Detail.Navigation.PopModalAsync();
                    else
                        return rootPage.Detail.Navigation.PopAsync();
                }
            }

            else
                return isModelPage
                    ? Application.Current.MainPage?.Navigation.PopModalAsync()
                                     : Application.Current.MainPage?.Navigation.PopAsync();

        }

        protected Page CreateAndBindPage(Type viewModelType)
        {
            if (!mapping.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for {viewModelType} was found on navigation mapping");
            }
            var pageType = mapping[viewModelType];
            var page = Activator.CreateInstance(pageType) as Page;
            var viewModel = Locator.Instance.Resolve(viewModelType);
            page.BindingContext = viewModel;
            return page;
        }

        void CreatePageViewModelMapping()
        {
            //mapping.Add(typeof(StartupViewModel), typeof(StartupPage));
            //mapping.Add(typeof(RootViewModel), typeof(RootPage));
            mapping.Add(typeof(LoginViewModel), typeof(LoginPage));
            mapping.Add(typeof(RegisterViewModel), typeof(RegisterPage));
            mapping.Add(typeof(ForgotPasswordViewModel), typeof(ForgotPasswordPage));
            mapping.Add(typeof(ForgotUserIdViewModel), typeof(ForgotUserIdPage));
            mapping.Add(typeof(PhraseViewModel), typeof(PhrasePage));
            mapping.Add(typeof(RootViewModel), typeof(RootPage));
            mapping.Add(typeof(ChangePasswordViewModel), typeof(ChangePasswordPage));
            mapping.Add(typeof(GoogleAuthenticatorViewModel), typeof(GoogleAuthenticatorPage));
            mapping.Add(typeof(SetLanguageViewModel), typeof(SetLanguagePage));
            mapping.Add(typeof(DashBoardViewModel), typeof(DashBoardPage));
            mapping.Add(typeof(CardViewModel), typeof(CardPage));
            mapping.Add(typeof(CardDetailsViewModel), typeof(CardDetailsPage));
            mapping.Add(typeof(NotificationViewModel), typeof(NotificationPage));
            mapping.Add(typeof(PaymentViewModel), typeof(PaymentPage));
            mapping.Add(typeof(ProjectHistoryViewModel), typeof(ProjectHistoryPage));
            mapping.Add(typeof(ProjectWalletViewModel), typeof(ProjectWalletPage));
            mapping.Add(typeof(PopupViewModel), typeof(WalletPopupPage));
          
            mapping.Add(typeof(LevelViewModel), typeof(LevelPage));
            mapping.Add(typeof(MatchingViewModel), typeof(MatchingPage));
            mapping.Add(typeof(ReferralViewModel), typeof(ReferralPage));



            mapping.Add(typeof(TransactionViewModel), typeof(TransactionPage));
            mapping.Add(typeof(ConvertHistoryViewModel), typeof(ConvertHistoryPage));
            mapping.Add(typeof(ConvertViewModel), typeof(Page));
            mapping.Add(typeof(ReceiveViewModel), typeof(ReceivePage));
            mapping.Add(typeof(SendViewModel), typeof(SendPage));
            mapping.Add(typeof(TransactionHistoryViewModel), typeof(TransactionHistoryPage));
            mapping.Add(typeof(SettingsViewModel), typeof(SettingsPage));

            mapping.Add(typeof(SideMenuViewModel), typeof(SideMenuPage));
            mapping.Add(typeof(SupportViewModel), typeof(SupportPage));

            mapping.Add(typeof(CardSidesViewModel), typeof(CardSidesPage));
            mapping.Add(typeof(MultiConvertViewModel), typeof(MultiConvertPage));
            mapping.Add(typeof(MnemonicPhraseViewModel), typeof(MnemonicPhrasePage));
            //mapping.Add(typeof(SecurityViewModel), typeof(SecurityPage));
            //mapping.Add(typeof(QrCodePopupViewModel), typeof(QrCodePopupPage));
            //mapping.Add(typeof(WithdrawViewModel), typeof(WithdrawPage));
            //mapping.Add(typeof(DepositViewModel), typeof(DepositPage));
            //mapping.Add(typeof(OrderHistoryViewModel), typeof(OrderHistoryPage));
            //mapping.Add(typeof(OrderViewModel), typeof(OrderPage));
            //mapping.Add(typeof(ProfileViewModel), typeof(ProfilePage));
            //mapping.Add(typeof(HomeViewModel), typeof(HomePage));
            //mapping.Add(typeof(MarketViewModel), typeof(MarketPage));
            //mapping.Add(typeof(TradeViewModel), typeof(TradePage));
            //mapping.Add(typeof(WalletViewModel), typeof(WalletPage));
            //mapping.Add(typeof(TransactionHistoryViewModel), typeof(TransactionHistoryPage));
            //mapping.Add(typeof(FeeViewModel), typeof(FeePage));
            //mapping.Add(typeof(CoinPairViewModel), typeof(CoinPairPage));
            //mapping.Add(typeof(SupportViewModel), typeof(SupportPage));
            //mapping.Add(typeof(SupportHistoryViewModel), typeof(SupportHistoryPage));
            //mapping.Add(typeof(ChangeRequestDetailViewModel), typeof(ChangeRequestDetailPage));
            //mapping.Add(typeof(ReplyPopupViewModel), typeof(ReplyPopupPage));
        }
    }
}