﻿using System;
using System.Threading.Tasks;
using HitToken.Module.DTOs;

namespace HitToken.Services.Transactions
{
    public interface ITransactionService
    {
        Task<TransactionHistoryResponse> GetTransactionHistoryAsync(string currency_type);
        Task<ResponseBase> SendTokenAsync(string currency_type,string amount,string to_address);
        Task<TransactionHistoryResponse> ConvertCurrencyAsync(string currency_type, string amount);
        Task<TransactionHistoryResponse> ConvertCurrencyHistoryAsync(string currency_type);
    }
}
