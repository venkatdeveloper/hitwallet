﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Module.DTOs;
using HitToken.Services.Request;

namespace HitToken.Services.Transactions
{
    public class TransactionServices : ITransactionService
    {
       
        readonly IRequestService requestService;
        public TransactionServices(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<TransactionHistoryResponse> ConvertCurrencyAsync(string currency_type, string amount)
        {
            var request = new RestRequest(Constants.CONVERT_CURRENCY_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { currency_type, amount });
            return requestService.ExecuteAsync<TransactionHistoryResponse>(request);
        }

        public Task<TransactionHistoryResponse> ConvertCurrencyHistoryAsync(string currency_type)
        {
            var request = new RestRequest(Constants.CONVERT_CURRENCY_HISTORY_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { currency_type });
            return requestService.ExecuteAsync<TransactionHistoryResponse>(request);
        }

        public Task<TransactionHistoryResponse> GetTransactionHistoryAsync(string currency_type)
        {
            var request = new RestRequest(Constants.TRANSACTION_HISTORY_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { currency_type });
            return requestService.ExecuteAsync<TransactionHistoryResponse>(request);
        }

        public Task<ResponseBase> SendTokenAsync(string currency_type, string amount, string to_address)
        {
            var request = new RestRequest(Constants.SEND_TOKEN_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { currency_type , amount , to_address });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }
    }
}
