﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace HitToken.Services.Dialog
{
    public class DialogService : IDialogService
    {
        public Task ShowAlertAsync(string title, string message, string cancel)
            => Application.Current.MainPage.DisplayAlert(title, message, cancel);

        public Task<bool> ShowConfimationAsync(string title, string message, string accept, string cencel)
            => Application.Current.MainPage.DisplayAlert(title, message, accept, cencel);

        public Task<string> ShowActionSheetAsync(string title, string cancel, string destruction, params string[] buttons)
            => Application.Current.MainPage.DisplayActionSheet(title, cancel, destruction, buttons);
    }
}