﻿using System;
using System.Threading.Tasks;
using HitToken.Module.DTOs;
using HitToken.Module.Register.Model;

namespace HitToken.Services.Authendication

{
    public interface IAuthendicationServices
    {
        Task<ResponseBase> VerifyUseridAsync(string userid);
        Task<LoginResponse> MemberLoginAsync(string userid, string password);
        Task<LoginResponse> MemberLoginAsync(string userid, bool isFingerprintSuccess);
        Task<ResponseBase> RecoverUserIdAsync(string emailid);
        Task<ResponseBase> RecoverPasswordAsync(string emailid);
        Task<MnemonicResponse> GetMneMonicPhraseAsync(string userid);
        Task<ResponseBase> CheckMnemonicPhraseAsync(string[] phrase, string userid);
        Task<ResponseBase> VerifyReferralCodeAsync(string referralid);
        Task<ResponseBase> CheckVerificationCodeAsync(string verify_code, string emailid);

        Task<ResponseBase> RegisterAsync(RegisterRequestModel requestModel, int step);
        Task<ResponseBase> ForgotUserIdPhraseAsync(string mnemonic);
        Task<ResponseBase> ForgotPasswordPhraseAsync(string mnemonic);

        //Task<ResponseBase> MemberRegisterationAsync(string referralid, string email_id, string loginPassword, string paymentpassword);
    }
}
