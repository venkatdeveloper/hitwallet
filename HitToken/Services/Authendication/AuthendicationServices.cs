﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Module.DTOs;
using HitToken.Module.Register.Model;
using HitToken.Services.Request;

namespace HitToken.Services.Authendication
{
    public class AuthendicationServices : IAuthendicationServices
    {
        readonly IRequestService requestService;

        public AuthendicationServices(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<ResponseBase> VerifyUseridAsync(string userid)
        {
            var request = new RestRequest(Constants.VERIFY_USER_ID_API, HttpMethod.Post);
            request.AddBody(new { username=userid });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<LoginResponse> MemberLoginAsync(string userid, string password)
        {
            var request = new RestRequest(Constants.LOGIN_API, HttpMethod.Post);
            request.AddBody(new { userid = userid, password });
            return requestService.ExecuteAsync<LoginResponse>(request);
        }

        public Task<ResponseBase> CheckVerificationCodeAsync(string verify_code, string emailid)
        {
            var request = new RestRequest(Constants.CHECK_VERIFY_CODE, HttpMethod.Post);
            request.AddBody(new { verifycode = verify_code, emailid = emailid });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<MnemonicResponse> GetMneMonicPhraseAsync(string userid)
        {
            var request = new RestRequest(Constants.GET_MNEMONIC_PHRASE_API, HttpMethod.Post);
            request.AddBody(new { userid = userid });
            return requestService.ExecuteAsync<MnemonicResponse>(request);
        }

    
        public Task<LoginResponse> MemberLoginAsync(string userid, bool isFingerprintSuccess)
        {
            var request = new RestRequest(Constants.LOGIN_API, HttpMethod.Post);
            request.AddBody(new { userid, isfingerprintsuccess = isFingerprintSuccess });
            return requestService.ExecuteAsync<LoginResponse>(request);
        }

        public Task<ResponseBase> ForgotUserIdPhraseAsync(string mnemonic)
        {
            var request = new RestRequest(Constants.FORGOT_USERID_API, HttpMethod.Post);
            request.AddBody(new { mnemonic});
            return requestService.ExecuteAsync<ResponseBase>(request);
        }


        public Task<ResponseBase> ForgotPasswordPhraseAsync(string mnemonic)
        {
            var request = new RestRequest(Constants.FORGOTPASSWORD_PHRASE_API, HttpMethod.Post);
            request.AddBody(new { mnemonic });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }




        public Task<ResponseBase> RegisterAsync(RegisterRequestModel requestModel, int step)
        {
            string api = Constants.REGISTRATION_FIRST_STEP_API;
            switch (step)
            {
                case 1:
                    {
                        api = Constants.REFERRAL_API;
                        break;
                    }
                case 2:
                    {
                        api = Constants.REGISTRATION_EMAIL_API;
                        break;
                    }
                case 3:
                    {
                        api = Constants.REGISTRATION_VERIFY_API;
                        break;
                    }
                case 4:
                    {
                        api = Constants.REGISTRATION_LOGIN_PWD_API;
                        break;
                    }
                case 5:
                    {
                        api = Constants.REGISTRATION_PAYMENT_PWD_API;
                        break;
                    }
            }
            var request = new RestRequest(api, HttpMethod.Post);
            request.AddBody(new { requestModel });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }


        public Task<ResponseBase> RecoverPasswordAsync(string emailid)
        {
            var request = new RestRequest(Constants.FORGOTPASSWORD_API, HttpMethod.Post);
            request.AddBody(new { email_id = emailid });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> RecoverUserIdAsync(string emailid)
        {
            var request = new RestRequest(Constants.FORGOT_USERID_API, HttpMethod.Post);
            request.AddBody(new { email_id = emailid });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> VerifyReferralCodeAsync(string referralid)
        {
            var request = new RestRequest(Constants.VERIFY_REFERRAL_CODE, HttpMethod.Post);
            request.AddBody(new { referralid });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

      
        public Task<ResponseBase> CheckMnemonicPhraseAsync(string[] phrase, string userid)
        {
            var request = new RestRequest(Constants.VERIFY_USER_ID_API, HttpMethod.Post);
            request.AddBody(new { userid, phrase = phrase });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        
    }
}
