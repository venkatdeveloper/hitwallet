﻿using System;
using System.Threading.Tasks;
using HitToken.Module.DTOs;

namespace HitToken.Services.Support
{
    public interface ISupportServices
    {
        Task<ResponseBase> CreateNewChangeRequestAsync(string details);
        Task<ResponseBase> AddCardDetailsAsync(string frontPath, string backPath);
        Task<ResponseBase> RechargeWalletAsync(string name, string amount);
        Task<NotificationResponse> GetNotificationAsync();
        Task<ResponseBase> CreateTwoFaAsync();
        Task<ResponseBase> VerifyTwoFaAsync(string payment_password, string google_code);
        
    }
}
