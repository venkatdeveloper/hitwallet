﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Module.DTOs;
using HitToken.Services.Request;

namespace HitToken.Services.Support
{
    public class SupportServices : ISupportServices
    {

        readonly IRequestService requestService;
        public SupportServices(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<ResponseBase> AddCardDetailsAsync(string frontPath, string backPath)
        {
            var request = new RestRequest(Constants.ADD_CARD_DETAILS_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            var files = new Dictionary<string, string>();
            files.Add("front_image", frontPath);
            files.Add("back_image", backPath);
            return requestService.UploadAsync<ResponseBase>(request, files);
        }

        public Task<ResponseBase> CreateNewChangeRequestAsync(string details)
        {
            var request = new RestRequest(Constants.NEW_CHANGE_REQUEST_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { details = details });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> CreateTwoFaAsync()
        {
            var request = new RestRequest(Constants.CREATE_TWO_FA_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<NotificationResponse> GetNotificationAsync()
        {
            var request = new RestRequest(Constants.GET_NOTIFICATIONS_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);

            return requestService.ExecuteAsync<NotificationResponse>(request);
        }

        public Task<ResponseBase> RechargeWalletAsync(string name, string amount)
        {
            var request = new RestRequest(Constants.RECHARGE_MY_WALLET, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { name, amount });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> VerifyTwoFaAsync(string payment_password, string google_code)
        {
            var request = new RestRequest(Constants.VERIFY_TWO_FA_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { payment_password, google_code });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }
    }
}
