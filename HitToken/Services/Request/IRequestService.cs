﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HitToken.Module.DTOs;

namespace HitToken.Services.Request
{
    public interface IRequestService
    {
        Task<TResult> ExecuteAsync<TResult>(IRestRequest request, string baseUrl = null) where TResult : ResponseBase;
        Task<TResult> UploadAsync<TResult>(IRestRequest request, Dictionary<string, string> files, string baseUrl = null) where TResult : ResponseBase;
    }
}