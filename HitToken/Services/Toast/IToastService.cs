﻿
namespace HitToken.Services.Toast
{
    public interface IToastService
    {
        void ShowToast(string message);
    }
}