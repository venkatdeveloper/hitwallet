﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Module.DTOs;
using HitToken.Services.Request;

namespace HitToken.Services.Account
{
    public class AccountServices : IAccountServices
    {
        readonly IRequestService requestService;
        public AccountServices(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<ResponseBase> ActivateAiProjectAsync(string amount, string symbol)
        {
            var request = new RestRequest(Constants.ACTIVATE_AI_PROJECT_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { amount, coin_symbol = symbol });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> ChangePassword(string oldPassword, string newPassword , string passwordType)
        {
            var request = new RestRequest(Constants.CHANGE_PASSWORD_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { oldpassword = oldPassword, newpassword = newPassword, passwordtype = passwordType });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<DashboardResponse> GetDashboardDetailsAsync()
        {
            var request = new RestRequest(Constants.GET_DASHBOARD_DETAILS_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<DashboardResponse>(request);
        }

        public Task<ReferralUsersResponse> GetLevelUsersAsync(string userlevel)
        {
            var request = new RestRequest(Constants.GET_LEVEL_USERS_LIST_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { userlevel });
            return requestService.ExecuteAsync<ReferralUsersResponse>(request);
        }

        public Task<MatchingUsersResponse> GetMatchingUsersUsersAsync()
        {
            var request = new RestRequest(Constants.GET_MATCHING_USERS_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<MatchingUsersResponse>(request);
        }

        public Task<PaymentsResponse> GetPaymentsListAsync()
        {
            var request = new RestRequest(Constants.GET_PAYMENT_LIST_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<PaymentsResponse>(request);
        }

        public Task<ProjectHistoryResponse> GetProjectHistoryAsync()
        {
            var request = new RestRequest(Constants.GET_PROJECT_HISTORY_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ProjectHistoryResponse>(request);
        }
    }
}
