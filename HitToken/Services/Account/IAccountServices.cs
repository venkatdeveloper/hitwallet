﻿using System;
using System.Threading.Tasks;
using HitToken.Module.DTOs;

namespace HitToken.Services.Account
{
    public interface IAccountServices
    {
        Task<DashboardResponse> GetDashboardDetailsAsync();
        Task<ResponseBase> ActivateAiProjectAsync(string amount,string symbol);
        Task<ReferralUsersResponse> GetLevelUsersAsync(string userlevel);
        Task<MatchingUsersResponse> GetMatchingUsersUsersAsync();
        Task<PaymentsResponse> GetPaymentsListAsync();
        Task<ProjectHistoryResponse> GetProjectHistoryAsync();
        Task<ResponseBase> ChangePassword(string oldPassword, string newPassword,string passwordType);
    }
}
