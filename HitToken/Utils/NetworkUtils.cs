﻿using Xamarin.Essentials;

namespace HitToken.Utils
{
    public static class NetworkUtils
    {
        public static bool IsConnected => Connectivity.NetworkAccess == NetworkAccess.Internet;
    }
}