﻿using HitToken.Module.DTOs;

namespace HitToken.Extension
{
    public static class ResponseBaseExtension
    {
        public static bool IsSuccess(this ResponseBase responseBase)
        {
            return responseBase.Message.Equals("success", System.StringComparison.CurrentCultureIgnoreCase);
        }
    }
}