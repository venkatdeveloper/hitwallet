﻿using System;
using System.Threading.Tasks;
using HitToken.Module.Common;
using HitToken.Services.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HitToken
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Initialize();
        }

        Task Initialize()
        {
            return Locator.Instance.Resolve<INavigationService>().InitializeAsync();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
